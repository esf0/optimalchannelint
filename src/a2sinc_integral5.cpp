//
// Created by esf0 on 12.02.2020.
//

#include "include/a2sinc_integral5.h"

double GetInt5Core(double y1, void* par) {

    Int5y1* p = (Int5y1*)par;
    double y2 = p->y2;
    double y4 = p->y4;
    double y5 = p->y5;

    std::complex<double> value = GetG2((y2 + y1) * (y2 - y1), (y4 - (y4 + y5 + 0.5)) * (y4 + (y4 + y5 + 0.5)), p->a2p) * GetEx(y4, 0.5 + y4 + y5, y2, y1, p->a2p);
    if (p->type == 0)
        return value.real();
    else
        return value.imag();
}

double GetInt5Y1(double y2, void* par) {

    Int5y2* p = (Int5y2*)par;
    double y4 = p->y4;
    double y5 = p->y5;

    double a = -0.5 + abs(y2 + 2. * y4 + 2. * y5 + 0.5);
    double b = 0.5 - abs(y2 + 2. * y4 + 2. * y5 + 0.5);
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    Int5y1 params = {p->a2p, p->type, y5, y4, y2};
    integrand.function = &GetInt5Core;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[3], p->a2p.eps_rel_ratio[3], n_intervals, p->a2p.key_ratio[3], workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetInt5Y2(double y4, void* par) {

    Int5y4* p = (Int5y4*)par;
    double y5 = p->y5;

    double a, b;

    -0.5 - y4 - y5 > -1. - 2. * (y4 + y5) ? a = -0.5 - y4 - y5 : a = -1. - 2. * (y4 + y5);
    -y4 - y5 < -2. * (y4 + y5) ? b = -y4 - y5 : b = -2. * (y4 + y5);
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    Int5y2 params = {p->a2p, p->type, y5, y4};
    integrand.function = &GetInt5Y1;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[2], p->a2p.eps_rel_ratio[2], n_intervals, p->a2p.key_ratio[2], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetInt5Y4N0(double y5, void* par) {

    Int5y5* p = (Int5y5*)par;

    double a = -0.25 - y5 * 0.5, b = 0.25 - y5 * 0.5;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    Int5y4 params = {p->a2p, p->type, y5};
    integrand.function = &GetInt5Y2;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[1], p->a2p.eps_rel_ratio[1], n_intervals, p->a2p.key_ratio[1], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result * y5;
}

double GetInt5Y4Nnot0(double y5, void* par) {

    Int5y5* p = (Int5y5*)par;

    double a = -0.25 - y5 * 0.5, b = 0.25 - y5 * 0.5;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    std::complex<double> result;
    for (int type = 0; type <= 1; ++type) {
        gsl_function integrand;
        Int5y4 params = {p->a2p, type, y5};
        integrand.function = &GetInt5Y2;
        integrand.params = &params;

        size_t n_intervals = p->a2p.n_intervals;
        gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(n_intervals);

        double result_temp = 0.;
        double abs_err = 0.;
        int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[1], p->a2p.eps_rel_ratio[1], n_intervals, p->a2p.key_ratio[1],
                                            workspace, &result_temp, &abs_err);
        if (type == 0)
            result.real(result_temp);
        else
            result.imag(result_temp);

        gsl_integration_workspace_free(workspace);
    }

    if (p->type == 0)
        return sign * (result.real() * cos(2. * M_PI * GetN(p->a2p) * y5) - result.imag() * sin(2. * M_PI * GetN(p->a2p) * y5));
    else
        return sign * (result.real() * sin(2. * M_PI * GetN(p->a2p) * y5) + result.imag() * cos(2. * M_PI * GetN(p->a2p) * y5));
}

double GetInt5Y5N0(A2SincParameters a2p, int type) {

    double a = -0.5, b = 0.5;

    double L = b - a;
    double dl = L / (double)a2p.n_fraction;
    b = a + dl * (a2p.ind_fraction + 1);
    a = a + dl * a2p.ind_fraction;

    gsl_function integrand;
    Int5y5 params = {a2p, type};
    integrand.function = &GetInt5Y4N0;
    integrand.params = &params;

    size_t n_intervals = a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs, a2p.eps_rel, n_intervals, a2p.key, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs_ratio[0], a2p.eps_rel_ratio[0], n_intervals, a2p.key_ratio[0], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}

double GetInt5Y5Nnot0(A2SincParameters a2p, int type) {

    double a = -0.5, b = 0.5;

    double L = b - a;
    double dl = L / (double)a2p.n_fraction;
    b = a + dl * (a2p.ind_fraction + 1);
    a = a + dl * a2p.ind_fraction;

    gsl_function integrand;
    Int5y5 params = {a2p, type};
    integrand.function = &GetInt5Y4Nnot0;
    integrand.params = &params;

    size_t n_intervals = a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs, a2p.eps_rel, n_intervals, a2p.key, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs_ratio[0], a2p.eps_rel_ratio[0], n_intervals, a2p.key_ratio[0], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}