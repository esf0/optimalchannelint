//
// Created by esf0 on 15.06.2020.
//
#include "integrals.h"

int GetN(Int1Parameters p) {
    return p.n + p.m - p.p - p.k;
}

double Get1IntCore(double t, void* par) {
    auto p_t = (IntT*)par;
    double y = p_t->y;
    int n = p_t->par.n;
    int m = p_t->par.m;
    int p = p_t->par.p;
    int k = p_t->par.k;

    std::complex<double> result;
    std::complex<double> g_first;
    std::complex<double> g_second;

    if (p_t->par.integral_type == 0) {
        g_first = GetG(p_t->par.t_beta * y * t);
    } else {
        g_first = GetG1(p_t->par.t_beta * y * t);
    }
    g_second = std::conj(g_first);

    if (p_t->par.N == 0) {
        result = (1. - y) * (
        g_first * cos(M_PI_2 * (double)(k - p) * (t + y)) *
        cos(M_PI_2 * (t * (double)(k + p - 2 * m) + y * (double)(k + p - 2 * n))) -
        g_second * cos(M_PI_2 * (double)(m - n) * (t + y)) *
        cos(M_PI_2 * (t * (double)(m + n - 2 * p) + y * (double)(m + n - 2 * k)))
                            );
    } else {
        result = g_first * cos(M_PI_2 * (double)(k - p) * (t + y)) *
        sin(M_PI_2 * (t * (double)(k + p - 2 * m) + y * (double)(k + p - 2 * n))) +
        g_second * cos(M_PI_2 * (double)(m - n) * (t + y)) *
        sin(M_PI_2 * (t * (double)(m + n - 2 * p) + y * (double)(m + n - 2 * k)));
    }

    if (p_t->par.complex_type == 0) {
        return result.real();
    } else {
        return result.imag();
    }
}

double Get1IntT(double y, void* par) {
    auto p = (Int1Parameters*)par;

    double a = 0;
    double b = 2. - y;

    gsl_function integrand;
    IntT params = {*p, y};
    integrand.function = &Get1IntCore;
    integrand.params = &params;

    size_t n_intervals = p->n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->eps_abs, p->eps_rel, n_intervals, p->key, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}

double Get1IBase(Int1Parameters par) {

    double a = 0., b = 2.;

    gsl_function integrand;
    integrand.function = &Get1IntT;
    integrand.params = &par;

    size_t n_intervals = par.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, par.eps_abs, par.eps_rel, n_intervals, par.key, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);
    return result;
}

std::complex<double> Get1I(Int1Parameters par) {

    int N = GetN(par);
    par.N = N;

    std::complex<double> result;

    par.complex_type = 0;
    result.real(Get1IBase(par));
    par.complex_type = 1;
    result.imag(Get1IBase(par));


    std::complex<double> coef(0.,0.);
    if (N == 0) {
        coef.imag(1./2.);
    } else {
        coef.imag(pow(-1., (double)N) / (2. * M_PI * (double)N));
    }
    return coef * result;

}