//
// Created by esf0 on 12.02.2020.
//

#include "include/a2sinc_integral2.h"


double GetInt2Core(double y1, void* par) {

    Int2y1* p = (Int2y1*)par;
    double y2 = p->y2;
    double y3 = p->y3;
    double y4 = p->y4;

    std::complex<double> value = GetG2((y2 + y1) * (y2 - y1), (y4 - y3) * (y4 + y3), p->a2p) * GetEx(y4, y3, y2, y1, p->a2p);
    if (p->type == 0)
        return value.real();
    else
        return value.imag();
}

double GetInt2Y1(double y2, void* par) {

    Int2y2* p = (Int2y2*)par;
    double y3 = p->y3;
    double y4 = p->y4;

    double a = -0.5 + abs(-0.5 + y2 + y3 + y4);
    double b = 0.5 - abs(-0.5 + y2 + y3 + y4);
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    Int2y1 params = {p->a2p, p->type, y4, y3, y2};
    integrand.function = &GetInt2Core;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, p->a2p.key, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[3], p->a2p.eps_rel_ratio[3], n_intervals, p->a2p.key_ratio[3], workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetInt2Y2(double y3, void* par) {

    Int2y3* p = (Int2y3*)par;
    double y4 = p->y4;

    double a, b;

    -(y3 + y4) / 2. > -(y3 + y4) ? a = -(y3 + y4) / 2. : a = -(y3 + y4);
    0.5 - (y3 + y4) / 2. < 1. - (y3 + y4) ? b = 0.5 - (y3 + y4) / 2. : b = 1. - (y3 + y4);
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    Int2y2 params = {p->a2p, p->type, y4, y3};
    integrand.function = &GetInt2Y1;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, p->a2p.key, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[2], p->a2p.eps_rel_ratio[2], n_intervals, p->a2p.key_ratio[2], workspace, &result, &abs_err);


    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetInt2Y3(double y4, void* par) {

    Int2y4* p = (Int2y4*)par;

    double a = -1. + y4, b = y4;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    Int2y3 params = {p->a2p, p->type, y4};
    integrand.function = &GetInt2Y2;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, p->a2p.key, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[1], p->a2p.eps_rel_ratio[1], n_intervals, p->a2p.key_ratio[1], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetInt2Y4(A2SincParameters a2p, int type) {

    double a = 0., b = 0.5;

    double L = b - a;
    double dl = L / (double)a2p.n_fraction;
    b = a + dl * (a2p.ind_fraction + 1);
    a = a + dl * a2p.ind_fraction;

    gsl_function integrand;
    Int2y4 params = {a2p, type};
    integrand.function = &GetInt2Y3;
    integrand.params = &params;

    size_t n_intervals = a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs, a2p.eps_rel, n_intervals, a2p.key, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs_ratio[0], a2p.eps_rel_ratio[0], n_intervals, a2p.key_ratio[0], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}