//
// Created by esf0 on 12.02.2020.
//

#include "include/b2sinc_integral7.h"

double GetBInt7Core(double y1, void* par) {

    BInt7y1* p = (BInt7y1*)par;
    double y3 = p->y3;
    double y4 = p->y4;
    double y5 = p->y5;

    std::complex<double> value = GetG3(((0.25 - 0.5 * (y3 + y4 + y5)) + y1) * ((0.25 - 0.5 * (y3 + y4 + y5)) - y1), (y4 - y3) * (y4 + y3), p->b2p) * GetExK(y4, y3, (0.25 - 0.5 * (y3 + y4 + y5)), y1, p->b2p);
    if (p->type == 0)
        return value.real();
    else
        return value.imag();
}

double GetBInt7Y1(double y3, void* par) {

    BInt7y3* p = (BInt7y3*)par;
    double y4 = p->y4;
    double y5 = p->y5;

    if (0.5 - (y3 + y4 + y5) <= 0)
        return 0.;

    double a = -0.5 + abs((0.25 + 0.5 * (y3 + y4 + y5)));
    double b = 0.5 - abs((0.25 + 0.5 * (y3 + y4 + y5)));
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    BInt7y1 params = {p->b2p, p->type, y5, y4, y3};
    integrand.function = &GetBInt7Core;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs_ratio[3], p->b2p.eps_rel_ratio[3], n_intervals, p->b2p.key_ratio[3], workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetBInt7Y3(double y4, void* par) {

    BInt7y4* p = (BInt7y4*)par;
    double y5 = p->y5;

    double a = -0.5 + y4 + y5;
    double b = 0.5 + y4 + y5;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    BInt7y3 params = {p->b2p, p->type, y5, y4};
    integrand.function = &GetBInt7Y1;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs_ratio[2], p->b2p.eps_rel_ratio[2], n_intervals, p->b2p.key_ratio[2], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetBInt7Y4N0(double y5, void* par) {

    BInt7y5* p = (BInt7y5*)par;

    double a = -0.25 - y5 * 0.5, b = 0.25 - y5 * 0.5;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    BInt7y4 params = {p->b2p, p->type, y5};
    integrand.function = &GetBInt7Y3;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs_ratio[1], p->b2p.eps_rel_ratio[1], n_intervals, p->b2p.key_ratio[1], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result * y5;
}

double GetBInt7Y4Nnot0(double y5, void* par) {

    BInt7y5* p = (BInt7y5*)par;

    double a = -0.25 - y5 * 0.5, b = 0.25 - y5 * 0.5;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    std::complex<double> result;
    for (int type = 0; type <= 1; ++type) {
        gsl_function integrand;
        BInt7y4 params = {p->b2p, type, y5};
        integrand.function = &GetBInt7Y3;
        integrand.params = &params;

        size_t n_intervals = p->b2p.n_intervals;
        gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(n_intervals);

        double result_temp = 0.;
        double abs_err = 0.;
        int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs_ratio[1], p->b2p.eps_rel_ratio[1], n_intervals, p->b2p.key_ratio[1],
                                            workspace, &result_temp, &abs_err);
        if (type == 0)
            result.real(result_temp);
        else
            result.imag(result_temp);

        gsl_integration_workspace_free(workspace);
    }

    if (p->type == 0)
        return sign * (result.real() * cos(2. * M_PI * GetN(p->b2p) * y5) - result.imag() * sin(2. * M_PI * GetN(p->b2p) * y5));
    else
        return sign * (result.real() * sin(2. * M_PI * GetN(p->b2p) * y5) + result.imag() * cos(2. * M_PI * GetN(p->b2p) * y5));
}

double GetBInt7Y5N0(B2SincParameters b2p, int type) {

    double a = -0.5, b = 0.5;

    double L = b - a;
    double dl = L / (double)b2p.n_fraction;
    b = a + dl * (b2p.ind_fraction + 1);
    a = a + dl * b2p.ind_fraction;

    gsl_function integrand;
    BInt7y5 params = {b2p, type};
    integrand.function = &GetBInt7Y4N0;
    integrand.params = &params;

    size_t n_intervals = b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, b2p.eps_abs_ratio[0], b2p.eps_rel_ratio[0], n_intervals, b2p.key_ratio[0], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}

double GetBInt7Y5Nnot0(B2SincParameters b2p, int type) {

    double a = -0.5, b = 0.5;

    double L = b - a;
    double dl = L / (double)b2p.n_fraction;
    b = a + dl * (b2p.ind_fraction + 1);
    a = a + dl * b2p.ind_fraction;

    gsl_function integrand;
    BInt7y5 params = {b2p, type};
    integrand.function = &GetBInt7Y4Nnot0;
    integrand.params = &params;

    size_t n_intervals = b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, b2p.eps_abs_ratio[0], b2p.eps_rel_ratio[0], n_intervals, b2p.key_ratio[0], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}