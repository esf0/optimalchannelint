//
// Created by esf0 on 12.02.2020.
//

#include "include/b2sinc_integral1.h"

double GetBInt1Core(double y1, void* par) {

    BInt1y1* p = (BInt1y1*)par;
    double y2 = p->y2;
    double y3 = p->y3;
    double y4 = p->y4;

    std::complex<double> value = GetG3((y2 + y1) * (y2 - y1), (y4 - y3) * (y4 + y3), p->b2p) * GetExK(y4, y3, y2, y1, p->b2p);
    if (p->type == 0)
        return value.real();
    else
        return value.imag();
}

double GetBInt1Y1(double y2, void* par) {

    BInt1y2* p = (BInt1y2*)par;
    double y3 = p->y3;
    double y4 = p->y4;

    double a = -0.5 + abs(0.5 + y2 + y3 + y4);
    double b = 0.5 - abs(0.5 + y2 + y3 + y4);
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    BInt1y1 params = {p->b2p, p->type, y4, y3, y2};
    integrand.function = &GetBInt1Core;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs_ratio[3], p->b2p.eps_rel_ratio[3], n_intervals, p->b2p.key_ratio[3], workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetBInt1Y2(double y3, void* par) {

    BInt1y3* p = (BInt1y3*)par;
    double y4 = p->y4;

    double a, b;

    -0.5 - (y3 + y4) / 2. > -1. - (y3 + y4) ? a = -0.5 - (y3 + y4) / 2. : a = -1. - (y3 + y4);
    -(y3 + y4) / 2. > -(y3 + y4) ? b = -(y3 + y4) : b = -(y3 + y4) / 2.;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    BInt1y2 params = {p->b2p, p->type, y4, y3};
    integrand.function = &GetBInt1Y1;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs_ratio[2], p->b2p.eps_rel_ratio[2], n_intervals, p->b2p.key_ratio[2], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetBInt1Y3(double y4, void* par) {

    BInt1y4* p = (BInt1y4*)par;

    double a = y4, b = 1. + y4;
    double sign = 1.0;
    if (a > b)
        swap(&a, &b, &sign);

    gsl_function integrand;
    BInt1y3 params = {p->b2p, p->type, y4};
    integrand.function = &GetBInt1Y2;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs_ratio[1], p->b2p.eps_rel_ratio[1], n_intervals, p->b2p.key_ratio[1], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return sign * result;
}

double GetBInt1Y4(B2SincParameters b2p, int type) {

    double a = -0.5, b = 0.;

    double L = b - a;
    double dl = L / (double)b2p.n_fraction;
    b = a + dl * (b2p.ind_fraction + 1);
    a = a + dl * b2p.ind_fraction;

    gsl_function integrand;
    BInt1y4 params = {b2p, type};
    integrand.function = &GetBInt1Y3;
    integrand.params = &params;

    size_t n_intervals = b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    int int_state = gsl_integration_qag(&integrand, a, b, b2p.eps_abs_ratio[0], b2p.eps_rel_ratio[0], n_intervals, b2p.key_ratio[0], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}