//
// Created by esf0 on 06.04.2020.
//

#include "include/a2sinc_check_integral_5d.h"

double GetIntC5dCore(double z2, void* par) {
    IntC5d_z2* p = (IntC5d_z2*)par;
    double t1 = p->t1;
    double t2 = p->t2;
    double z1 = p->z1;
    int* m = p->a2p.m;
    double t_beta = p->a2p.t_beta;

    std::complex<double> im = 1i;
    std::complex<double> value = pow(t1, 1.5) * pow(t2, 0.5) * pow(cos(z1), -2.) * pow(cos(z2), -2.) *
            exp( -im * M_PI * M_PI * pow(tan(z1) - tan(z2), 2) * 0.5 / t_beta + im * M_PI * 0.25) *
            GetE5d(2. * M_PI * (sqrt(t1 * t2) * tan(z2) + m[0]), 2. * t_beta * t1 * (1 - t2) ) *
            GetE5d(2. * M_PI * (sqrt(t1 * t2) * tan(z2) + m[1]), 2. * t_beta * t1 * (1 - t2) ) *
            GetE5d(2. * M_PI * (sqrt(t1 * t2) * tan(z1) + m[2]), 2. * t_beta * t1 ) *
            conj(GetE5d(2. * M_PI * (sqrt(t1 * t2) * tan(z2) + m[3]), 2. * t_beta * t1 * (1 - t2) )) *
            conj(GetE5d(2. * M_PI * (sqrt(t1 * t2) * tan(z1) + m[4]), 2. * t_beta * t1 )) *
            conj(GetE5d(2. * M_PI * (sqrt(t1 * t2) * tan(z1) + m[5]), 2. * t_beta * t1 ));

    if (p->type == 0)
        return value.real();
    else
        return value.imag();
}

double GetIntC5d_z2(double z1, void* par) {

    IntC5d_z1* p = (IntC5d_z1*)par;
    double t1 = p->t1;
    double t2 = p->t2;

//    double a = -M_PI_2;
//    double b = M_PI_2;
    double a = p->a2p.z_int_bot;
    double b = p->a2p.z_int_up;

    gsl_function integrand;
    IntC5d_z2 params = {p->a2p, p->type, t1, t2, z1};
    integrand.function = &GetIntC5dCore;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, p->a2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs * 0.001, p->a2p.eps_rel * 0.001, n_intervals, p->a2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[3], p->a2p.eps_rel_ratio[3], n_intervals, p->a2p.key_ratio[3], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;

}

double GetIntC5d_z1(double t2, void* par) {

    IntC5d_t2* p = (IntC5d_t2*)par;
    double t1 = p->t1;

//    double a = -M_PI_2;
//    double b = M_PI_2;
    double a = p->a2p.z_int_bot;
    double b = p->a2p.z_int_up;


    gsl_function integrand;
    IntC5d_z1 params = {p->a2p, p->type, t1, t2};
    integrand.function = &GetIntC5d_z2;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, p->a2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs * 0.01, p->a2p.eps_rel * 0.01, n_intervals, p->a2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[2], p->a2p.eps_rel_ratio[2], n_intervals, p->a2p.key_ratio[2], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;
}

double GetIntC5d_t2(double t1, void* par) {

    IntC5d_t1* p = (IntC5d_t1*)par;

//    double a = 0.;
//    double b = 1.;
    double a = p->a2p.t_int_bot;
    double b = p->a2p.t_int_up;

    gsl_function integrand;
    IntC5d_t2 params = {p->a2p, p->type, t1};
    integrand.function = &GetIntC5d_z1;
    integrand.params = &params;

    size_t n_intervals = p->a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, p->a2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs * 0.1, p->a2p.eps_rel * 0.1 , n_intervals, p->a2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->a2p.eps_abs, p->a2p.eps_rel, n_intervals, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, p->a2p.eps_abs_ratio[1], p->a2p.eps_rel_ratio[1], n_intervals, p->a2p.key_ratio[1], workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;

}

double GetIntC5d_t1(A2SincParameters a2p, int type) {

//    double a = 0.;
//    double b = 1.;
    double a = a2p.t_int_bot;
    double b = a2p.t_int_up;

    gsl_function integrand;
    IntC5d_t1 params = {a2p, type};
    integrand.function = &GetIntC5d_t2;
    integrand.params = &params;

    size_t n_intervals = a2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
//    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs, a2p.eps_rel, n_intervals, a2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, a2p.eps_abs, a2p.eps_rel, n_intervals, workspace, &result, &abs_err);
    int int_state = gsl_integration_qag(&integrand, a, b, a2p.eps_abs_ratio[0], a2p.eps_rel_ratio[0], n_intervals, a2p.key_ratio[0], workspace, &result, &abs_err);


    gsl_integration_workspace_free(workspace);

    return sqrt(M_PI * 0.5 / a2p.t_beta) * result;


}