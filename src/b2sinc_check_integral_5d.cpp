//
// Created by esf0 on 06.04.2020.
//

#include "include/b2sinc_check_integral_5d.h"

double GetIntCB5dCore(double z, void* par) {
    IntCB5d_z* p = (IntCB5d_z*)par;
    double z1 = p->z1;
    double z2 = p->z2;
    double a_var = p->a;
    int* k = p->b2p.k;
    double t_beta = p->b2p.t_beta;

    std::complex<double> im = 1i;
    std::complex<double> z_sqrt = z * sqrt(2. * t_beta * im * (z2 - z1));
    std::complex<double> b_z1 = 2. * t_beta * z1;
    std::complex<double> b_z2 = 2. * t_beta * z2;



    std::complex<double> sum = 0.;
    for (int r = -p->b2p.M; r <= p->b2p.M; ++r) {
        sum += GetE5d(2. * M_PI * (double)r + a_var + z_sqrt, b_z1) * conj(GetE5d(conj(2. * M_PI * (double)r + a_var - z_sqrt), b_z2));
    }

    std::complex<double> result = GetE5d(2. * M_PI * (double)k[0] + a_var - z_sqrt, b_z2) *
            GetE5d(2. * M_PI * (double)k[1] + a_var - z_sqrt, b_z2) *
            conj(GetE5d(conj(2. * M_PI * (double)k[2] + a_var + z_sqrt), b_z1)) *
            conj(GetE5d(conj(2. * M_PI * (double)k[3] + a_var + z_sqrt), b_z1)) *
            sum * sqrt(M_1_PI) * exp(-pow(z, 2));

    if (p->type == 0)
        return result.real();
    else
        return result.imag();
}

double GetIntCB5d_z(double a_var, void* par) {

    IntCB5d_a* p = (IntCB5d_a*)par;
    double z1 = p->z1;
    double z2 = p->z2;

    double a = p->b2p.z_int_bot;
    double b = p->b2p.z_int_up;

    gsl_function integrand;
    IntCB5d_z params = {p->b2p, p->type, z1, z2, a_var};
    integrand.function = &GetIntCB5dCore;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    size_t neval;
    int int_state = gsl_integration_qng(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, &result, &abs_err, &neval);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, p->b2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs * 0.001, p->b2p.eps_rel * 0.001, n_intervals, p->b2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result * M_1_PI / 2.;

}

double GetIntCB5d_a(double z2, void* par) {

    IntCB5d_z2* p = (IntCB5d_z2*)par;
    double z1 = p->z1;

    double a = p->b2p.a_int_bot;
    double b = p->b2p.a_int_up;

    gsl_function integrand;
    IntCB5d_a params = {p->b2p, p->type, z1, z2};
    integrand.function = &GetIntCB5d_z;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    size_t neval;
    int int_state = gsl_integration_qng(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, &result, &abs_err, &neval);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, p->b2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs * 0.01, p->b2p.eps_rel * 0.01, n_intervals, p->b2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result * std::min(z1, z2);
}

double GetIntCB5d_z2(double z1, void* par) {

    IntCB5d_z1* p = (IntCB5d_z1*)par;

    double a = 0.;
    double b = 1.;

    gsl_function integrand;
    IntCB5d_z2 params = {p->b2p, p->type, z1};
    integrand.function = &GetIntCB5d_a;
    integrand.params = &params;

    size_t n_intervals = p->b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    size_t neval;
    int int_state = gsl_integration_qng(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, &result, &abs_err, &neval);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, p->b2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qag(&integrand, a, b, p->b2p.eps_abs * 0.1, p->b2p.eps_rel * 0.1 , n_intervals, p->b2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, p->b2p.eps_abs, p->b2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;

}

double GetIntCB5d_z1(B2SincParameters b2p, int type) {

    double a = 0.;
    double b = 1.;

    gsl_function integrand;
    IntCB5d_z1 params = {b2p, type};
    integrand.function = &GetIntCB5d_z2;
    integrand.params = &params;

    size_t n_intervals = b2p.n_intervals;
    gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(n_intervals);

    double result = 0.;
    double abs_err = 0.;
    size_t neval;
    int int_state = gsl_integration_qng(&integrand, a, b, b2p.eps_abs, b2p.eps_rel, &result, &abs_err, &neval);
//    int int_state = gsl_integration_qag(&integrand, a, b, b2p.eps_abs, b2p.eps_rel, n_intervals, b2p.key, workspace, &result, &abs_err);
//    int int_state = gsl_integration_qags(&integrand, a, b, b2p.eps_abs, b2p.eps_rel, n_intervals, workspace, &result, &abs_err);

    gsl_integration_workspace_free(workspace);

    return result;


}