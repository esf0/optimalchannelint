#include <iostream>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <stdlib.h>
#include <omp.h>
//#include <exception>
#include <csignal>
#include <random>

#include "integrals.h"
#include "gsl/gsl_sf_trig.h"
#include "gsl/gsl_complex.h"
#include "gsl/gsl_complex_math.h"

int calculate_int_clu(int argc, char** argv) {

    int m[6] = {0, 0, 0, 0, 0, 0};
    double eps_rel = 1e-3;
    double eps_abs = 1e-3;
    int m_max_value = 3;
    int m_var_value = 2 * m_max_value + 1;
    int m_total = pow(m_var_value, 6);
    int m_start = 0;
    int m_end = m_total - 1; 

    if (argc == 1) {
        std::cout << "No input parameters" << std::endl;
        return 1;
    } else if (argc < 6) {
        std::cout << "Not enough input parameters" << std::endl;
        return 2;
    } else {
        m_max_value = atoi(argv[1]);
        m_start = atoi(argv[2]);
        m_end = atoi(argv[3]);    
        eps_rel = atof(argv[4]);
        eps_abs = atof(argv[5]);
    }

    if (m_end > m_total - 1) {
        m_end = m_total - 1;
    }
    

    A2SincParameters par;
    for (int k = 0; k < 6; ++k)
        par.m[k] = m[k];
    par.N = GetN(par);
    par.eps_rel = eps_rel;
    par.eps_abs = eps_abs;
    par.a_start = -1.0;
    par.a_end = 1.0;
    par.b_start = -1.0;
    par.b_end = 1.0;
    par.n_integral = pow(2, 12);
    par.t_beta = 1.; // Only for check that everything works. NOT FOR CALCULATIONS!!!
    par.n_intervals = 1000; // Only for check that everything works. NOT FOR CALCULATIONS!!!


    // Calculate Integrals


    int m_var;
//    #pragma omp parallel for schedule(auto) default(none) private(par, m_var) shared(m_total, m_var_value, m_max_value, m_start, m_end, std::cout, eps_rel, eps_abs)
    #pragma omp parallel for schedule(auto) num_threads(15) default(none) private(par, m_var) shared(m_total, m_var_value, m_max_value, m_start, m_end, std::cout, eps_rel, eps_abs)
    for (m_var = m_start; m_var <= m_end; ++m_var) {

        int m_index[6];
        m_index[0] = m_var % m_var_value - m_max_value;
        m_index[1] = (m_var / m_var_value) % m_var_value - m_max_value;
        m_index[2] = ((m_var / m_var_value) / m_var_value) % m_var_value - m_max_value;
        m_index[3] = (((m_var / m_var_value) / m_var_value) / m_var_value) % m_var_value - m_max_value;
        m_index[4] = ((((m_var / m_var_value) / m_var_value) / m_var_value) / m_var_value) % m_var_value - m_max_value;
        m_index[5] =
                (((((m_var / m_var_value) / m_var_value) / m_var_value) / m_var_value) / m_var_value) % m_var_value -
                m_max_value;


        for (int k = 0; k < 6; ++k) {
            par.m[k] = m_index[k];
        }
        par.N = GetN(par);
        par.eps_rel = eps_rel;
        par.eps_abs = eps_abs;

        if ( (par.N == 0) || (par.m[0] == -m_max_value)) {
            
            par.key = GSL_INTEG_GAUSS21;
    
            par.t_beta = 1.; // Only for check that everything works. NOT FOR CALCULATIONS!!!
            par.n_intervals = 10000; // Only for check that everything works. NOT FOR CALCULATIONS!!!
    
    
            std::vector<std::complex<double>> integrals_values(9);
            std::vector<int> integrals_ms(9);
    
            auto time_point_one = std::chrono::steady_clock::now();
            auto time_point_two = std::chrono::steady_clock::now();
            auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
    
            for (int k = 1; k <= 7; ++k) {
                time_point_one = std::chrono::steady_clock::now();
                integrals_values[k - 1] = GetI(k, par);
                time_point_two = std::chrono::steady_clock::now();
                diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
                integrals_ms[k - 1] = (int) diff_ms.count();
            }
    
            time_point_one = std::chrono::steady_clock::now();
            integrals_values[7] = GetI(9, par);
            time_point_two = std::chrono::steady_clock::now();
            diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
            integrals_ms[7] = (int) diff_ms.count();
    
            time_point_one = std::chrono::steady_clock::now();
            integrals_values[8] = GetI(11, par);
            time_point_two = std::chrono::steady_clock::now();
            diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
            integrals_ms[8] = (int) diff_ms.count();
    
    
            // Write to file
    
            std::stringstream file_name;
            file_name << "result_M_5/result_parallel_";
            for (int k = 0; k < 6; ++k)
                file_name << par.m[k] << "_";
            file_name << eps_rel << "_" << eps_abs << ".txt";
    
            std::fstream file;
            file.open(file_name.str(), std::ios::out | std::ios::app);
    
            for (int k = 0; k < 6; ++k)
                file << par.m[k] << " ";
            file << eps_rel << " " << eps_abs << std::endl;
    
            file << std::setprecision(20);
    
            double real_total = 0., imag_total = 0.;
            for (int k = 0; k < 9; ++k) {
                file << integrals_values[k].real() << " " << integrals_values[k].imag() << " " << integrals_ms[k]
                     << std::endl;
                real_total += integrals_values[k].real();
                imag_total += integrals_values[k].imag();
            }
            file << real_total << " " << imag_total << std::endl;
    
    
            file.close();
        }
    }

    delete par.g2_m;
    return 0;
}

int calculate_int_by_sum(int argc, char** argv) {

    int m[6] = {0, 0, 0, 0, 0, 0};
    double eps_rel = 1e-3;
    double eps_abs = 0.;

    if (argc == 1) {
        std::cout << "No input parameters" << std::endl;
        return 1;
    } else if (argc < 9) {
        std::cout << "Not enough input parameters" << std::endl;
        return 2;
    } else {
        for (int k = 0; k < 6; ++k)
            m[k] = atoi(argv[k+1]);
        eps_rel = atof(argv[7]);
        eps_abs = atof(argv[8]);
    }

    std::cout << "Input parameters: ";
    for (int k = 0; k < 6; ++k)
        std::cout << m[k] << " ";
    std::cout << "; relative error: " << eps_rel << ", absolute error: " << eps_abs << std::endl;

    A2SincParameters par;
    for (int k = 0; k < 6; ++k)
        par.m[k] = m[k];
    par.N = GetN(par);
    par.eps_rel = eps_rel;
    par.eps_abs = eps_abs;

    par.a_start = -1.0;
    par.a_end = 1.0;
    par.b_start = -1.0;
    par.b_end = 1.0;
    par.n_integral = pow(2, 12);

    par.t_beta = 1.; // Only for check that everything works. NOT FOR CALCULATIONS!!!
    par.n_intervals = 1000; // Only for check that everything works. NOT FOR CALCULATIONS!!!

//    auto time_point_one = std::chrono::steady_clock::now();
////    par.g2_m = GetG2Grid(par); // 2^14 is a maximum for my computer (16Gb memory)
//    auto time_point_two = std::chrono::steady_clock::now();
//    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
//    std::cout << "It tooks " << diff_ms.count() << "ms" << std::endl;


    // Calculate Integrals

    int m_max_value = 3;
    int m_var_value = 2 * m_max_value + 1;
    int m_total = pow(m_var_value, 6);

//    omp_set_num_threads(16);
//    std::cout << omp_get_num_threads() << std::endl;

    // ONLY FOR TEST
//    m_total = 200;
//    std::mt19937 gen(m[0]);
//    std::uniform_int_distribution<> dis(-4, 4);
//
//    int m_matrix[6][200];
//    std::ifstream f_m_input("m_index.txt");
//    if (f_m_input.is_open()) {
//        for (int i_line = 0; i_line < 200; i_line++) {
//            for (int i_pos = 0; i_pos < 6; i_pos++) {
//                std::string buff;
//                f_m_input >> buff;
//                m_matrix[i_pos][i_line] = atoi(buff.c_str());
////                std::cout << m_matrix[i_pos][i_line] << " ";
//            }
////            std::cout << std::endl;
//        }
//        f_m_input.close();
//    }
//
//    return 0;
    // -------------

    int m_var;
//    #pragma omp parallel for schedule(auto) num_threads(10) default(none) private(par, m_var) shared(m_matrix, m_total, m_var_value, m_max_value, std::cout, dis, gen, eps_rel, eps_abs)
    #pragma omp parallel for schedule(auto) num_threads(15) default(none) private(par, m_var) shared(m_total, m_var_value, m_max_value, std::cout, eps_rel, eps_abs)
    for (m_var = 0; m_var < m_total; ++m_var) {
//        std::cout << m_var << " start" << std::endl;
//        std::cout << m_var << " start" << " ";

        int m_index[6];
        m_index[0] = m_var % m_var_value - m_max_value;
        m_index[1] = (m_var / m_var_value) % m_var_value - m_max_value;
        m_index[2] = ((m_var / m_var_value) / m_var_value) % m_var_value - m_max_value;
        m_index[3] = (((m_var / m_var_value) / m_var_value) / m_var_value) % m_var_value - m_max_value;
        m_index[4] = ((((m_var / m_var_value) / m_var_value) / m_var_value) / m_var_value) % m_var_value - m_max_value;
        m_index[5] =
                (((((m_var / m_var_value) / m_var_value) / m_var_value) / m_var_value) / m_var_value) % m_var_value -
                m_max_value;

        // ONLY FOR TEST
//        std::cout << "Test index" << std::endl;
//        for (int i_m_index = 0; i_m_index < 6; ++i_m_index) {
//            m_index[i_m_index] = i_m_index;
//            m_index[i_m_index] = dis(gen);
//            m_index[i_m_index] = m_matrix[i_m_index][m_var];
//            std::cout << m_index[i_m_index] << " ";
//        }
//        std::cout << std::endl;
        // -------------


//        std::cout << "Test index 2" << std::endl;
        for (int k = 0; k < 6; ++k) {
            par.m[k] = m_index[k];
//            std::cout << m_index[k] << " " << par.m[k] << " ";
        }
//        std::cout << std::endl << "Test index 3" << std::endl;
        par.N = GetN(par);
        par.eps_rel = eps_rel;
        par.eps_abs = eps_abs;

        if ( (par.N == 0) || (par.m[0] == -m_max_value)) {
            
    
    //        par.a_start = -1.0;
    //        par.a_end = 1.0;
    //        par.b_start = -1.0;
    //        par.b_end = 1.0;
    //        par.n_integral = pow(2, 12);
            par.key = GSL_INTEG_GAUSS31;
    
            par.t_beta = 1.; // Only for check that everything works. NOT FOR CALCULATIONS!!!
            par.n_intervals = 10000; // Only for check that everything works. NOT FOR CALCULATIONS!!!
    
            //PrintA2SincParameters(par);
            // ------------------
    //        std::cout << "t_beta = " << par.t_beta << std::endl;
    
    //        std::cout << "m = ";
    //        for (int i = 0; i < 6; ++i) {
    //            std::cout << par.m[i] << " "; 
    //        }   
    //        std::cout << std::endl;
        
    //        std::cout << "eps_abs = " << par.eps_abs << std::endl;
    //        std::cout << "eps_rel = " << par.eps_rel << std::endl;
    //        std::cout << "key = " << par.key << std::endl;
    //        std::cout << "n_intervals = " << par.n_intervals << std::endl;
    //        std::cout << "a_start = " << par.a_start << std::endl;
    //        std::cout << "a_end = " << par.a_end << std::endl;
    //        std::cout << "b_start = " << par.b_start << std::endl;
    //        std::cout << "b_end = " << par.b_end << std::endl;
    //        std::cout << "n_integral = " << par.n_integral << std::endl;
    //        std::cout << "g2_m = " << par.g2_m << std::endl;
            
            // ------------------
    
    
            std::vector<std::complex<double>> integrals_values(9);
            std::vector<int> integrals_ms(9);
    
            auto time_point_one = std::chrono::steady_clock::now();
            auto time_point_two = std::chrono::steady_clock::now();
            auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
    
            for (int k = 1; k <= 7; ++k) {
                time_point_one = std::chrono::steady_clock::now();
                integrals_values[k - 1] = GetI(k, par);
                time_point_two = std::chrono::steady_clock::now();
                diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
                integrals_ms[k - 1] = (int) diff_ms.count();
            }
    
            time_point_one = std::chrono::steady_clock::now();
            integrals_values[7] = GetI(9, par);
            time_point_two = std::chrono::steady_clock::now();
            diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
            integrals_ms[7] = (int) diff_ms.count();
    
            time_point_one = std::chrono::steady_clock::now();
            integrals_values[8] = GetI(11, par);
            time_point_two = std::chrono::steady_clock::now();
            diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
            integrals_ms[8] = (int) diff_ms.count();
    
    //    std::cout << std::setprecision(20) << GetI1(par) << std::endl;
    //    std::cout << "It tooks " << diff_ms.count() << "ms" << std::endl;
    
            // Write to file
    
            std::stringstream file_name;
            file_name << "result_M_5/result_parallel_";
            for (int k = 0; k < 6; ++k)
                file_name << par.m[k] << "_";
            file_name << eps_rel << "_" << eps_abs << ".txt";
    
            std::fstream file;
            file.open(file_name.str(), std::ios::out | std::ios::app);
    
            for (int k = 0; k < 6; ++k)
                file << par.m[k] << " ";
            file << eps_rel << " " << eps_abs << std::endl;
    
            file << std::setprecision(20);
    
            double real_total = 0., imag_total = 0.;
            for (int k = 0; k < 9; ++k) {
                file << integrals_values[k].real() << " " << integrals_values[k].imag() << " " << integrals_ms[k]
                     << std::endl;
                real_total += integrals_values[k].real();
                imag_total += integrals_values[k].imag();
            }
            file << real_total << " " << imag_total << std::endl;
    
    
            file.close();
    //        std::cout << m_var << " end" << std::endl;
    
        }
    }

    delete par.g2_m;
    return 0;
}

int check_calculations(int argc, char** argv) {

    int m[6];
    double eps_rel = 1e-3;
    double eps_abs = 0.;
    int calc_key = 0;

    if (argc == 1) {
        std::cout << "No input parameters" << std::endl;
        return 1;
    } else if (argc < 9) {
        std::cout << "Not enough input parameters" << std::endl;
        return 2;
    } else {
        for (int k = 0; k < 6; ++k)
            m[k] = atoi(argv[k+1]);
        eps_rel = atof(argv[7]);
        eps_abs = atof(argv[8]);
    }
    if (argc == 10) {
        calc_key = atoi(argv[9]);
        std::cout << "Calculation: " << calc_key << std::endl;
    }

    std::cout << "Input parameters: ";
    for (int k = 0; k < 6; ++k)
        std::cout << m[k] << " ";
    std::cout << "; relative error: " << eps_rel << ", absolute error: " << eps_abs << std::endl;

    A2SincParameters par;
    for (int k = 0; k < 6; ++k)
        par.m[k] = m[k];
    par.N = GetN(par);
    par.eps_rel = eps_rel;
    par.eps_abs = eps_abs;

    for (int k = 0; k < 4; ++k) {
        par.eps_abs_ratio[k] = eps_abs;
        par.eps_rel_ratio[k] = eps_rel;
    }


    par.a_start = -1.0;
    par.a_end = 1.0;
    par.b_start = -1.0;
    par.b_end = 1.0;
    par.n_integral = pow(2, 13);
    par.key = GSL_INTEG_GAUSS61;
    par.key_ratio[0] = GSL_INTEG_GAUSS31;
    par.key_ratio[1] = GSL_INTEG_GAUSS31;
    par.key_ratio[2] = GSL_INTEG_GAUSS31;
    par.key_ratio[3] = GSL_INTEG_GAUSS61;

    par.t_beta = 1.; // Only for check that everything works. NOT FOR CALCULATIONS!!!
    par.n_intervals = 1000000; // Only for check that everything works. NOT FOR CALCULATIONS!!!

    auto time_point_one = std::chrono::steady_clock::now();
    par.g2_m = GetG2Grid(par); // 2^14 is a maximum for my computer (16Gb memory)
    auto time_point_two = std::chrono::steady_clock::now();
    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
    std::cout << "Grid takes " << diff_ms.count() << "ms" << std::endl;


    // Calculate Integrals

    for (int k = 0; k < 6; ++k)
        par.m[k] = m[k];

    std::vector<std::complex<double>> integrals_values(9);
    std::vector<int> integrals_ms(9);

//    auto time_point_one = std::chrono::steady_clock::now();
//    auto time_point_two = std::chrono::steady_clock::now();
//    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

    std::cout << std::setprecision(20) << std::endl;

    if (calc_key == 0 || calc_key == 1) {

        for (int k = 1; k <= 7; ++k) {
            time_point_one = std::chrono::steady_clock::now();
            integrals_values[k - 1] = GetI(k, par);
            time_point_two = std::chrono::steady_clock::now();
            diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
            integrals_ms[k - 1] = (int) diff_ms.count();
        }

        time_point_one = std::chrono::steady_clock::now();
        integrals_values[7] = GetI(9, par);
        time_point_two = std::chrono::steady_clock::now();
        diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
        integrals_ms[7] = (int) diff_ms.count();

        time_point_one = std::chrono::steady_clock::now();
        integrals_values[8] = GetI(11, par);
        time_point_two = std::chrono::steady_clock::now();
        diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
        integrals_ms[8] = (int) diff_ms.count();


        double real_total = 0., imag_total = 0.;
        for (int k = 0; k < 9; ++k) {
            std::cout << integrals_values[k].real() << " " << integrals_values[k].imag() << " " << integrals_ms[k]
                      << std::endl;
            real_total += integrals_values[k].real();
            imag_total += integrals_values[k].imag();
        }
        std::cout << real_total << " " << imag_total << std::endl;
    }

    // Calculate with 5d integral

    if (calc_key == 0 || calc_key == 2) {
        time_point_one = std::chrono::steady_clock::now();
        std::complex<double> integral_5d_value = GetA2with5d(par);
        time_point_two = std::chrono::steady_clock::now();
        diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
        int integral_5d_ms = (int) diff_ms.count();

        std::cout << integral_5d_value.real() << " " << integral_5d_value.imag() << " " << integral_5d_ms << std::endl;
    }
    delete par.g2_m;
    return 0;
}

int test_new_interface(int argc, char** argv) {

    std::vector<std::complex<double>> a_integrals_values(9);
    std::vector<int> a_integrals_ms(9);
    std::vector<std::complex<double>> b_integrals_values(9);
    std::vector<int> b_integrals_ms(9);

    A2SincParameters a_par;
    a_par.m[0] = 0;
    a_par.m[1] = 0;
    a_par.m[2] = 0;
    a_par.m[3] = 0;
    a_par.m[4] = 0;
    a_par.m[5] = 0;
    a_par.N = GetN(a_par);

    a_par.j_k = 1;
    a_par.j_m = 1;

    a_par.t_beta = 1.;
    a_par.key = GSL_INTEG_GAUSS61;
    a_par.eps_abs = 1e-3;
    a_par.eps_rel = 1e-3;
    a_par.sum_key = 3;

//    GetExSumBy2Var(0.249875, -0.25, 0.25, -0.25, a_par);
//    GetExSumBy2Var(-0.25, 0.25, -0.25, 0.249875, a_par);

//    return 0;

    B2SincParameters b_par;
    b_par.N = GetN(b_par);
    b_par.k[0] = 0;
    b_par.k[1] = 0;
    b_par.k[2] = -2;
    b_par.k[3] = 2;
    b_par.t_beta = 1.;
    b_par.key = GSL_INTEG_GAUSS15;
    b_par.eps_abs = 1e-2;
    b_par.eps_rel = 1e-2;


//    CalculateIntegrals(a_integrals_values, a_integrals_ms, a_par);
//    CalculateIntegrals(b_integrals_values, b_integrals_ms, b_par);
//    PrintIntegrals(a_integrals_values, a_integrals_ms);
//    PrintIntegrals(b_integrals_values, b_integrals_ms);

    CalculateJSumA2(a_integrals_values, a_integrals_ms, 1, 7, a_par);
    PrintIntegrals(a_integrals_values, a_integrals_ms);

//    std::string file_name("test_file_output.txt");
//    WriteIntegralsToFile(file_name, a_integrals_values, a_integrals_ms, a_par.m, 6, a_par.eps_rel, a_par.eps_abs);
//    WriteIntegralsToFile(file_name, b_integrals_values, b_integrals_ms, b_par.k, 4, a_par.eps_rel, a_par.eps_abs);


    return 0;
}

int test_ab1_integrals() {
    Int1Parameters par;
    par.t_beta = 0.;
    par.n = 0;
    par.m = 0;
    par.p = 1;
    par.k = 1;
//    par.eps_abs = 1e-8;
//    par.eps_rel = 1e-8;

    auto time_point_one = std::chrono::steady_clock::now();
    auto time_point_two = std::chrono::steady_clock::now();
    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

    par.integral_type = 0;
    time_point_one = std::chrono::steady_clock::now();
    auto result = Get1I(par);
    time_point_two = std::chrono::steady_clock::now();
    diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
    std::cout << result << " " << (int)diff_ms.count() << std::endl;
    par.integral_type = 1;
    result = Get1I(par);
    std::cout << result << " " << (int)diff_ms.count() << std::endl;

    return 0;
}

int test_b_5d() {

    B2SincParameters b_par;
    b_par.N = GetN(b_par);
    b_par.k[0] = -3;
    b_par.k[1] = -5;
    b_par.k[2] = -3;
    b_par.k[3] = -5;
    b_par.N = GetN(b_par);
    b_par.t_beta = 1.;
    b_par.key = GSL_INTEG_GAUSS61;
    b_par.eps_abs = 1e-3;
    b_par.eps_rel = 1e-3;
    b_par.a_int_up = 200.;
    b_par.a_int_bot = -200.;

    std::complex<double> result = 0.;

    std::cout << "Start" << std::endl;

    int n_z = pow(2, 2);
    int n_a = pow(2, 3);
    double z_span = pow(2, 6);
    double a_span = pow(2,8);
    double da = a_span / (double)n_a;
    double dz = z_span / (double)n_z;
    for (int iter_a = 0; iter_a < n_a; ++iter_a) {
        std::cout << iter_a << " :";
        b_par.a_int_bot = iter_a * da - a_span / 2.;
        b_par.a_int_up = (iter_a + 1) * da - a_span / 2.;
        for (int iter_z = 0; iter_z < n_z; ++iter_z) {
            std::cout << iter_z << " ";
            b_par.z_int_bot = iter_z * dz - z_span / 2.;
            b_par.z_int_up = (iter_z + 1) * dz - z_span / 2.;
            result += GetB2with5d(b_par);
        }
        std::cout << std::endl;
    }

    std::cout << "End" << std::endl;
    std::cout << result << std::endl;

    return 0;
}

int test_a2_5d() {

    A2SincParameters a_par;
//    a_par.m = {5, -5, 1, 1, -5, 5};
    a_par.m[0] = 5;
    a_par.m[1] = -5;
    a_par.m[2] = 1;
    a_par.m[3] = 1;
    a_par.m[4] = -5;
    a_par.m[5] = 5;
    a_par.N = GetN(a_par);
    a_par.t_beta = 5.;

    a_par.key_ratio[0] = GSL_INTEG_GAUSS21;
    a_par.key_ratio[1] = GSL_INTEG_GAUSS21;
    a_par.key_ratio[2] = GSL_INTEG_GAUSS31;
    a_par.key_ratio[3] = GSL_INTEG_GAUSS61;

    a_par.eps_abs_ratio[0] = 1e-2;
    a_par.eps_abs_ratio[1] = 1e-2;
    a_par.eps_abs_ratio[2] = 1e-2;
    a_par.eps_abs_ratio[3] = 1e-3;

    a_par.eps_rel_ratio[0] = 1e-2;
    a_par.eps_rel_ratio[1] = 1e-2;
    a_par.eps_rel_ratio[2] = 1e-2;
    a_par.eps_rel_ratio[3] = 1e-3;

    a_par.t_int_bot = 0.;
    a_par.t_int_up = 1.;
    a_par.z_int_bot = -M_PI_2;
    a_par.z_int_up = M_PI_2;

    std::complex<double> result = 0.;

    std::cout << "Start" << std::endl;

    // int n_t = pow(2, 2);
    // int n_z = pow(2, 4);
    // double t_span = 1.;
    // double z_span = M_PI * 0.99;
    // (4.82521e-07,-1.22943e-07)
    // Total worktime = 13211273 ms
//    Start
//    t:0
//    0 (3.92033e-10,3.02975e-11)
//    1 (1.19075e-09,4.55604e-12)
//    2 (2.36953e-09,5.96377e-11)
//    3 (3.93222e-09,1.05415e-10)
//    0 (4.51439e-09,1.24713e-10)
//    1 (4.948e-09,1.51556e-10)
//    2 (5.27624e-09,1.19346e-10)
//    3 (5.41534e-09,1.45257e-10)
//
//    t:1
//    0 (5.41534e-09,1.45257e-10)
//    1 (5.41535e-09,1.45258e-10)
//    2 (5.4154e-09,1.45258e-10)
//    3 (5.41558e-09,1.45272e-10)
//    0 (5.41573e-09,1.45283e-10)
//    1 (5.41577e-09,1.45283e-10)
//    2 (5.41578e-09,1.45284e-10)
//    3 (5.41578e-09,1.45284e-10)
//
//    t:2
//    0 (5.41578e-09,1.45284e-10)
//    1 (5.41579e-09,1.45284e-10)
//    2 (5.41579e-09,1.45284e-10)
//    3 (5.41581e-09,1.45285e-10)
//    0 (5.41583e-09,1.45286e-10)
//    1 (5.41583e-09,1.45286e-10)
//    2 (5.41583e-09,1.45286e-10)
//    3 (5.41583e-09,1.45286e-10)
//
//    t:3
//    0 (5.41583e-09,1.45286e-10)
//    1 (5.41583e-09,1.45286e-10)
//    2 (5.41584e-09,1.45286e-10)
//    3 (5.41584e-09,1.45286e-10)
//    0 (5.41584e-09,1.45286e-10)
//    1 (5.41585e-09,1.45286e-10)
//    2 (5.41585e-09,1.45286e-10)
//    3 (5.41585e-09,1.45286e-10)
//
//    End
//    (5.41585e-09,1.45286e-10)
//    Total worktime = 132535663 ms



    int n_t = pow(2, 2);
    int n_z = pow(2, 2);
    double t_span = 1.;
    double z_span = M_PI * 0.99;
    double dt = t_span / (double)n_t;
    double dz = z_span / (double)n_z;

//    for (int iter_t = 0; iter_t < n_t; ++iter_t) {
//        std::cout << iter_t << " :";
//        a_par.t_int_bot = iter_t * dt;
//        a_par.t_int_up = (iter_t + 1) * dt;
//        for (int iter_z = 0; iter_z < n_z; ++iter_z) {
//            std::cout << iter_z << " ";
//            a_par.z_int_bot = iter_z * dz - z_span / 2.;
//            a_par.z_int_up = (iter_z + 1) * dz - z_span / 2.;
//            result += GetA2with5d(a_par);
//        }
//        std::cout << std::endl;
//    }

    double z_span_1_bot = -M_PI_2 * 0.999;
    double z_span_1_up = -M_PI_2 * 0.99;
    double z_span_1 = z_span_1_up - z_span_1_bot;
    double dz_1 = z_span_1 / (double) n_z;

    double z_span_2_bot = M_PI_2 * 0.99;
    double z_span_2_up = M_PI_2 * 0.999;
    double z_span_2 = z_span_2_up - z_span_2_bot;
    double dz_2 = z_span_2 / (double) n_z;

    for (int iter_t = 0; iter_t < n_t; ++iter_t) {
        std::cout << "t:" << iter_t << std::endl;
        a_par.t_int_bot = iter_t * dt;
        a_par.t_int_up = (iter_t + 1) * dt;
        for (int iter_z_1 = 0; iter_z_1 < n_z; ++iter_z_1) {
            std::cout << iter_z_1 << " ";
            a_par.z_int_bot = iter_z_1 * dz_1 + z_span_1_bot;
            a_par.z_int_up = (iter_z_1 + 1) * dz_1 + z_span_1_bot;
            result += GetA2with5d(a_par);
            std::cout << result << std::endl;
        }
        for (int iter_z_2 = 0; iter_z_2 < n_z; ++iter_z_2) {
            std::cout << iter_z_2 << " ";
            a_par.z_int_bot = iter_z_2 * dz_2 + z_span_2_bot;
            a_par.z_int_up = (iter_z_2 + 1) * dz_2 + z_span_2_bot;
            result += GetA2with5d(a_par);
            std::cout << result << std::endl;
        }
        std::cout << std::endl;
    }

    std::cout << "End" << std::endl;
    std::cout << result << std::endl;

    return 0;
}

int calculate_ab1(double t_beta) {
    std::stringstream file_name;
    file_name << "result_ab1_" << t_beta << ".csv";
    std::ofstream file(file_name.str());

    int k_max_value = 15;
    int k_var_value = 2 * k_max_value + 1;
    int k_total = pow(k_var_value, 4);


    Int1Parameters par;
    par.t_beta = t_beta;
    par.eps_abs = 1e-4;
    par.eps_rel = 1e-4;

    auto time_point_one = std::chrono::steady_clock::now();
    auto time_point_two = std::chrono::steady_clock::now();
    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

    for (int i = 0; i < k_total; ++i) {
        int *k = CalculateIndex(i, k_max_value, k_var_value, 4);
        par.n = k[0];
        par.m = k[1];
        par.p = k[2];
        par.k = k[3];

        file << t_beta << "," << k[0] << "," << k[1] << "," << k[2] << "," << k[3] << ",";

        par.integral_type = 0;
        time_point_one = std::chrono::steady_clock::now();
        auto result = Get1I(par);
        time_point_two = std::chrono::steady_clock::now();
        diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        file << result.real() << "," << result.imag() << "," << (int)diff_ms.count() << ",";

        par.integral_type = 1;
        time_point_one = std::chrono::steady_clock::now();
        result = Get1I(par);
        time_point_two = std::chrono::steady_clock::now();
        diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        file << result.real() << "," << result.imag() << "," << (int)diff_ms.count() << std::endl;

        delete k;
    }

    file.close();

    return 0;
}

// TODO: rewrite inside (core and coef)
//int calculate_a2(const CalculationParameters par) {
//
////    const double eps_abs = 1e-3;
////    const double eps_rel = 1e-3;
////    const int key = GSL_INTEG_GAUSS61;
//
//    const double t_beta = par.t_beta;
//    const int m_max = par.m_max;
//
//    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
//    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
//    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};
//
//    // To calculate all index_start = 0; index_end = (2 * m_max + 1) * (m_max + 1);
//    #pragma omp parallel for num_threads(1) default(none) shared(par, t_beta, m_max, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio)
//    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {
//
//        // This cycle calculates index m and k
//        int m_calc;
//        int k_calc;
//        int index_cur = 0;
//        bool break_m = false;
//        for (int m = -m_max; m <= m_max; ++m) {
//            for (int k = -m_max; k <= m; ++k) {
//                if (index_cur == index_var) {
//                    m_calc = m;
//                    k_calc = k;
//                    break_m = true;
//                    break;
//                } else {
//                    index_cur++;
//                }
//            }
//            if (break_m) {
//                break;
//            }
//        }
//
//        std::cout << index_var << " " << m_calc << " " << k_calc << " ";
//        std::cout << t_beta << " " << m_max << " " << std::endl;
//
//        A2SincParameters a_par;
//        a_par.t_beta = t_beta;
//        a_par.M = m_max;
////        a_par.key = key;
////        a_par.eps_abs = eps_abs;
////        a_par.eps_rel = eps_rel;
//        a_par.n_fraction = par.n_fraction;
//        a_par.ind_fraction = par.ind_fraction;
//
//        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
//            a_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
//            a_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
//            a_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
////            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
//        }
//
//        std::vector<std::complex<double>> j_integrals_values(9);
//        std::vector<int> j_integrals_ms(9);
//
//        CalculateJSumA2(j_integrals_values, j_integrals_ms, m_calc, k_calc, a_par);
////            PrintIntegrals(j_integrals_values, j_integrals_ms);
//
//        std::stringstream file_name;
//        file_name << "result_J_a2_" << t_beta << "_" << m_max << "_" << m_calc << "_" << k_calc << ".txt";
//        std::string f_name = file_name.str();
//        WriteIntegralsToFile(f_name, j_integrals_values, j_integrals_ms, m_calc, k_calc, a_par.eps_rel_ratio,
//                             a_par.eps_abs_ratio, a_par.key_ratio);
//
//    }
//
//    return 0;
//}


int calculate_a2_pure(const CalculationParameters par) {

    const double t_beta = par.t_beta;
    const int m_max = par.m_max;

    const int k_max_value = m_max;
    const int k_var_value = 2 * k_max_value + 1;
    const int k_total = pow(k_var_value, 6);

    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};

    int n_integral = par.n_integral;

    if (par.calculation_type != 1000) {
        std::cout << "[calculate_a2_pure] Wrong calculation type" << std::endl;
        return -1;
    }

    // To calculate all index_start = 0; index_end = (2 * m_max + 1) * (m_max + 1);
#pragma omp parallel for num_threads(1) default(none) shared(par, t_beta, m_max, k_max_value, k_var_value, k_total, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio, n_integral)
    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {

        auto time_point_one = std::chrono::steady_clock::now();

        int s[6];
        for (int m_ind = 0; m_ind < 6; m_ind++)
            s[m_ind] = par.m[m_ind];


        std::cout << index_var << " " << s[0] << " " << s[1] << " " << s[2] << " " << s[3] << " " << s[4] << " "<< s[5] << " ";
        std::cout << t_beta << " " << m_max << " " << std::endl;

        A2SincParameters a_par;
        a_par.t_beta = t_beta;
        a_par.M = m_max;
        a_par.n_integral = n_integral;
        a_par.g2_m = par.g2_m;
        a_par.sum_key = 0;
        for (int m_ind = 0; m_ind < 6; m_ind++)
            a_par.m[m_ind] = s[m_ind];
        a_par.N = GetN(a_par);



        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
            a_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
            a_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
            a_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
//            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
        }

        std::vector<std::complex<double>> integrals_values(9);
        std::vector<int> integrals_ms(9);

//        CalculateSumTermA2(j_integrals_values, j_integrals_ms, s, a_par);
        CalculateIntegrals(integrals_values, integrals_ms, a_par);
//            PrintIntegrals(j_integrals_values, j_integrals_ms);

        std::stringstream file_name;
        file_name << "result_a2_pure_" << t_beta << "_" << m_max << "_" << s[0] << "_" << s[1]
        << "_" << s[2] << "_" << s[3] << "_" << s[4] << "_"<< s[5] << ".txt";

        std::string f_name = file_name.str();
        WriteIntegralsToFile(f_name, integrals_values, integrals_ms, s, 6, a_par.eps_rel_ratio,
                             a_par.eps_abs_ratio, a_par.key_ratio);

        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::ofstream log_file;
        log_file.open("log_a2_pure.txt", std::ios::out | std::ios::app);
        log_file << "[A2 term] [ " << index_var << " ][" << t_beta << "][" << m_max << "]["
                 << s[0] << "][" << s[1] << "][" << s[2] << "][" << s[3] << "][" << s[4] << "][" << s[5] << "]"
                 << " calculation time: " << (int)diff_ms.count() << " ms" << std::endl;
        log_file.close();
    }

    return 0;
}



int calculate_a2(const CalculationParameters par) {

    const double t_beta = par.t_beta;
    const int m_max = par.m_max;

    const int k_max_value = m_max;
    const int k_var_value = 2 * k_max_value + 1;
    const int k_total = pow(k_var_value, 2);

    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};

    int n_integral = par.n_integral;

    if (par.calculation_type != 104 && par.calculation_type != 105) {
        std::cout << "[calculate_a2] Wrong calculation type" << std::endl;
        return -1;
    }

    // To calculate all index_start = 0; index_end = (2 * m_max + 1) * (m_max + 1);
#pragma omp parallel for num_threads(1) default(none) shared(par, t_beta, m_max, k_max_value, k_var_value, k_total, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio, n_integral)
    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {

        auto time_point_one = std::chrono::steady_clock::now();

        // This cycle calculates index m and k
        int s[4];

        if (par.calculation_type == 104) {
            s[0] = 0;

            // Calc s[1,2,3]
            int *k = CalculateIndex(index_var, k_max_value, k_var_value, 3);
            s[1] = k[0];
            s[2] = k[1];
            s[3] = k[2];
            delete k;

        } else {
            int *k = CalculateHalfIndex(index_var, m_max, 4);
            for (int i = 0; i < 4; ++i) {
                s[i] = k[i];
            }
            delete k;
        }

        // This cycle calculates index m and k
//        int m_calc;
//        int k_calc;
//        int index_cur = 0;
//        bool break_m = false;
//        for (int m = -m_max; m <= m_max; ++m) {
//            for (int k = -m_max; k <= m; ++k) {
//                if (index_cur == index_var) {
//                    m_calc = m;
//                    k_calc = k;
//                    break_m = true;
//                    break;
//                } else {
//                    index_cur++;
//                }
//            }
//            if (break_m) {
//                break;
//            }
//        }


//        int *k = CalculateIndex(index_var, k_max_value, k_var_value, 2);
//
//        int m_calc = k[0];
//        int k_calc = k[1];

        std::cout << index_var << " " << s[0] << " " << s[1] << " " << s[2] << " " << s[3] << " ";
        std::cout << t_beta << " " << m_max << " " << std::endl;

        A2SincParameters a_par;
        a_par.t_beta = t_beta;
        a_par.M = m_max;
        a_par.n_integral = n_integral;
        a_par.g2_m = par.g2_m;
        a_par.sum_key = par.calculation_type;



        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
            a_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
            a_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
            a_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
//            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
        }

        std::vector<std::complex<double>> j_integrals_values(9);
        std::vector<int> j_integrals_ms(9);

        CalculateSumTermA2(j_integrals_values, j_integrals_ms, s, a_par);
//            PrintIntegrals(j_integrals_values, j_integrals_ms);

        std::stringstream file_name;
        if (par.calculation_type == 104) {
            file_name << "result_a2_in_term_J" << t_beta << "_" << m_max << "_" << s[0] << "_" << s[1] << "_" << s[2] << "_" << s[3] << ".txt";
        } else {
            file_name << "result_a2_out_term_J" << t_beta << "_" << m_max << "_" << s[0] << "_" << s[1] << "_" << s[2] << "_" << s[3] << ".txt";
        }
        std::string f_name = file_name.str();
        WriteIntegralsToFile(f_name, j_integrals_values, j_integrals_ms, s, 4, a_par.eps_rel_ratio,
                             a_par.eps_abs_ratio, a_par.key_ratio);

        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::ofstream log_file;
        if (par.calculation_type == 104) {
            log_file.open("log_a2_in_term.txt", std::ios::out | std::ios::app);
        } else {
            log_file.open("log_a2_out_term.txt", std::ios::out | std::ios::app);
        }
        log_file << "[A2 term] [ " << index_var << " ][" << t_beta << "][" << m_max << "]["
                 << s[0] << "][" << s[1] << "][" << s[2] << "][" << s[3] << "]"
                 << " calculation time: " << (int)diff_ms.count() << " ms" << std::endl;
        log_file.close();
    }

    return 0;
}

int calculate_a2_sum(const CalculationParameters par) {

    const double t_beta = par.t_beta;
    const int m_max = par.m_max;

    const int k_max_value = m_max;
    const int k_var_value = 2 * k_max_value + 1;
    const int k_total = pow(k_var_value, 2);

    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};

    int n_integral = par.n_integral;


    // To calculate all index_start = 0; index_end = (2 * m_max + 1) * (m_max + 1);
#pragma omp parallel for num_threads(1) default(none) shared(par, t_beta, m_max, k_max_value, k_var_value, k_total, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio, n_integral)
    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {

        auto time_point_one = std::chrono::steady_clock::now();

        // This cycle calculates index m and k
        int *k = CalculateIndex(index_var, k_max_value, k_var_value, 2);

        int m_calc = k[0];
        int k_calc = k[1];

        std::cout << index_var << " " << m_calc << " " << k_calc << " ";
        std::cout << t_beta << " " << m_max << " " << std::endl;

        A2SincParameters a_par;
        a_par.t_beta = t_beta;
        a_par.M = m_max;
        a_par.n_integral = n_integral;
        a_par.g2_m = par.g2_m;


        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
            a_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
            a_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
            a_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
//            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
        }

        std::vector<std::complex<double>> j_integrals_values(9);
        std::vector<int> j_integrals_ms(9);

        CalculateJSumA2(j_integrals_values, j_integrals_ms, m_calc, k_calc, a_par);
//            PrintIntegrals(j_integrals_values, j_integrals_ms);

        std::stringstream file_name;
        file_name << "result_a2_sum_J" << t_beta << "_" << m_max << "_" << m_calc << "_" << k_calc << ".txt";
        std::string f_name = file_name.str();
        WriteIntegralsToFile(f_name, j_integrals_values, j_integrals_ms, m_calc, k_calc, a_par.eps_rel_ratio,
                             a_par.eps_abs_ratio, a_par.key_ratio);

        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::ofstream log_file;
        log_file.open("log_a2_sum.txt", std::ios::out | std::ios::app);
        log_file << "[A2 MU index] [ " << index_var << " ][" << t_beta << "][" << m_max << "]["
                 << m_calc << "][" << k_calc << "]"
                 << " calculation time: " << (int)diff_ms.count() << " ms" << std::endl;
        log_file.close();
    }

    return 0;
}

int calculate_a2_for_mu(const CalculationParameters par) {

    const double t_beta = par.t_beta;
    const int m_max = par.m_max;

    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};

    int n_integral = par.n_integral;


    // To calculate all index_start = 0; index_end = (2 * m_max + 1) * (m_max + 1);
#pragma omp parallel for num_threads(1) default(none) shared(par, t_beta, m_max, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio, n_integral)
    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {

        auto time_point_one = std::chrono::steady_clock::now();

        // This cycle calculates index m and k
        // in this case (for MU) m = k
        int m_calc = index_var - m_max;
        int k_calc = m_calc;

        std::cout << index_var << " " << m_calc << " " << k_calc << " ";
        std::cout << t_beta << " " << m_max << " " << std::endl;

        A2SincParameters a_par;
        a_par.t_beta = t_beta;
        a_par.M = m_max;
        a_par.n_integral = n_integral;
        a_par.g2_m = par.g2_m;

        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
            a_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
            a_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
            a_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
//            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
        }

        std::vector<std::complex<double>> j_integrals_values(9);
        std::vector<int> j_integrals_ms(9);

        CalculateJSumA2(j_integrals_values, j_integrals_ms, m_calc, k_calc, a_par);
//            PrintIntegrals(j_integrals_values, j_integrals_ms);

        std::stringstream file_name;
        file_name << "result_a2_for_mu_J" << t_beta << "_" << m_max << "_" << m_calc << "_" << k_calc << ".txt";
        std::string f_name = file_name.str();
        WriteIntegralsToFile(f_name, j_integrals_values, j_integrals_ms, m_calc, k_calc, a_par.eps_rel_ratio,
                             a_par.eps_abs_ratio, a_par.key_ratio);

        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::ofstream log_file;
        log_file.open("log_a2_mu.txt", std::ios::out | std::ios::app);
        log_file << "[A2 MU index] [ " << index_var << " ][" << t_beta << "][" << m_max << "]["
                 << m_calc << "][" << k_calc << "]"
                 << " calculation time: " << (int)diff_ms.count() << " ms" << std::endl;
        log_file.close();
    }

    return 0;
}

int calculate_b2_wo_sum(const CalculationParameters par) {

    const double t_beta = par.t_beta;
    const int m_max = par.m_max;

    const int k_max_value = m_max;
    const int k_var_value = 2 * k_max_value + 1;
    const int k_total = pow(k_var_value, 4);

    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};

    std::vector<std::complex<double>>* g3_m = par.g3_m;
    int n_integral = par.n_integral;

    // To calculate all index_start = 0; index_end = (2 * m_max + 1)^4;
    #pragma omp parallel for num_threads(1) default(none) shared(par, t_beta, m_max, k_max_value, k_var_value, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio, g3_m, n_integral)
    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {

        auto time_point_one = std::chrono::steady_clock::now();

        B2SincParameters b_par;
        b_par.t_beta = t_beta;
        b_par.M = m_max;
        b_par.sum_key = 0;
        b_par.g3_m = g3_m;
        b_par.n_integral = n_integral;

        //

        int *k = CalculateIndex(index_var, k_max_value, k_var_value, 4);

        b_par.k[0] = k[0];
        b_par.k[1] = k[1];
        b_par.k[2] = k[2];
        b_par.k[3] = k[3];
        b_par.N = GetN(b_par);

        //

        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
            b_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
            b_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
            b_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
//            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
        }

        std::vector<std::complex<double>> j_integrals_values(9);
        std::vector<int> j_integrals_ms(9);

        CalculateIntegrals(j_integrals_values, j_integrals_ms, b_par);
//        CalculateJLambdaSumB2(j_integrals_values, j_integrals_ms, m_calc, k_calc, b_par);
//        PrintIntegrals(j_integrals_values, j_integrals_ms);

        std::stringstream file_name;
        file_name << "result_b2_4_index_" << t_beta << "_" << m_max << "_" << k[0] << "_" << k[1] << "_" << k[2] << "_" << k[3] << ".txt";
        std::string f_name = file_name.str();
        WriteIntegralsToFile(f_name, j_integrals_values, j_integrals_ms, k, 4, b_par.eps_rel_ratio,
                             b_par.eps_abs_ratio, b_par.key_ratio);

        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::ofstream log_file;
        log_file.open("log_b2_4.txt", std::ios::out | std::ios::app);
        log_file << "[B2 4 index] [ " << index_var << " ][" << t_beta << "][" << m_max << "]["
        << k[0] << "][" << k[1] << "][" << k[2] << "][" << k[3] << "]"
        << " calculation time: " << (int)diff_ms.count() << " ms" << std::endl;
        log_file.close();
    }

    return 0;
}

int calculate_b2_wo_sum_for_mu(const CalculationParameters par) {

    const double t_beta = par.t_beta;
    const int m_max = par.m_max;

    const int k_max_value = m_max;
    const int k_var_value = 2 * k_max_value + 1;
    const int k_total = pow(k_var_value, 2);

    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};

    std::vector<std::complex<double>>* g3_m = par.g3_m;
    int n_integral = par.n_integral;

#pragma omp parallel for num_threads(1) default(none) shared(par, t_beta, m_max, k_max_value, k_var_value, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio, g3_m, n_integral)
    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {

        auto time_point_one = std::chrono::steady_clock::now();

//        std::cout << index_var << " " << m_calc << " " << k_calc << " ";
//        std::cout << t_beta << " " << m_max << " " << std::endl;

        B2SincParameters b_par;
        b_par.t_beta = t_beta;
        b_par.M = m_max;
        b_par.sum_key = 0;
        b_par.g3_m = g3_m;
        b_par.n_integral = n_integral;

        //

        int *k = CalculateIndex(index_var, k_max_value, k_var_value, 2);

        b_par.k[0] = k[0];
        b_par.k[1] = k[1];
        b_par.k[2] = k[0];
        b_par.k[3] = k[1];
        b_par.N = GetN(b_par);
        std::cout << "ind = " << k[0] << " " << k[1] << std::endl;

        //


        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
            b_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
            b_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
            b_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
//            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
        }

        std::vector<std::complex<double>> j_integrals_values(9);
        std::vector<int> j_integrals_ms(9);

        CalculateIntegrals(j_integrals_values, j_integrals_ms, b_par);
//        CalculateJLambdaSumB2(j_integrals_values, j_integrals_ms, m_calc, k_calc, b_par);
//            PrintIntegrals(j_integrals_values, j_integrals_ms);

        std::stringstream file_name;
        file_name << "result_b2_for_mu_JLambda_" << t_beta << "_" << m_max << "_" << k[0] << "_" << k[1] << "_" << k[0] << "_" << k[1] << ".txt";
        std::string f_name = file_name.str();
        WriteIntegralsToFile(f_name, j_integrals_values, j_integrals_ms, k, 2, b_par.eps_rel_ratio,
                             b_par.eps_abs_ratio, b_par.key_ratio);

        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::ofstream log_file;
        log_file.open("log_b2_mu.txt", std::ios::out | std::ios::app);
        log_file << "[B2 MU index] [ " << index_var << " ][" << t_beta << "][" << m_max << "]["
                 << k[0] << "][" << k[1] << "][" << k[0] << "][" << k[1] << "]"
                 << " calculation time: " << (int)diff_ms.count() << " ms" << std::endl;
        log_file.close();
    }

    return 0;
}

int calculate_b2(const CalculationParameters par) {

//    const double eps_abs = 1e-3;
//    const double eps_rel = 1e-3;
//    const int key = GSL_INTEG_GAUSS61;

    const double t_beta = par.t_beta;
    const int m_max = par.m_max;

    const int key_ratio[4] = {par.key_ratio[0], par.key_ratio[1], par.key_ratio[2], par.key_ratio[3]};
    const double eps_abs_ratio[4] = {par.eps_abs_ratio[0], par.eps_abs_ratio[1], par.eps_abs_ratio[2], par.eps_abs_ratio[3]};
    const double eps_rel_ratio[4] = {par.eps_rel_ratio[0], par.eps_rel_ratio[1], par.eps_rel_ratio[2], par.eps_rel_ratio[3]};

    // To calculate all index_start = 0; index_end = (2 * m_max + 1) * (2 * m_max + 1);
    #pragma omp parallel for num_threads(12) default(none) shared(par, t_beta, m_max, std::cout, key_ratio, eps_abs_ratio, eps_rel_ratio)
    for (int index_var = par.index_start; index_var < par.index_end; ++index_var) {

        // This cycle calculates index m and k
        int m_calc;
        int k_calc;
        int index_cur = 0;
        bool break_m = false;
        for (int m = -m_max; m <= m_max; ++m) {
            for (int k = -m_max; k <= m_max; ++k) {
                if (index_cur == index_var) {
                    m_calc = m;
                    k_calc = k;
                    break_m = true;
                    break;
                } else {
                    index_cur++;
                }
            }
            if (break_m) {
                break;
            }
        }

//        std::cout << index_var << " " << m_calc << " " << k_calc << " ";
//        std::cout << t_beta << " " << m_max << " " << std::endl;

        B2SincParameters b_par;
        b_par.t_beta = t_beta;
        b_par.M = m_max;
//        b_par.key = key;
//        b_par.eps_abs = eps_abs;
//        b_par.eps_rel = eps_rel;
        b_par.n_fraction = par.n_fraction;
        b_par.ind_fraction = par.ind_fraction;

        for (int ratio_ind = 0; ratio_ind < 4; ++ratio_ind) {
            b_par.eps_abs_ratio[ratio_ind] = eps_abs_ratio[ratio_ind];
            b_par.eps_rel_ratio[ratio_ind] = eps_rel_ratio[ratio_ind];
            b_par.key_ratio[ratio_ind] = key_ratio[ratio_ind];
//            std::cout << eps_abs_ratio[ratio_ind] << " " << eps_rel_ratio[ratio_ind] << " " << key_ratio[ratio_ind] << std::endl;
        }

        std::vector<std::complex<double>> j_integrals_values(9);
        std::vector<int> j_integrals_ms(9);

//        CalculateIntegrals(j_integrals_values, j_integrals_ms, b_par);
        CalculateJLambdaSumB2(j_integrals_values, j_integrals_ms, m_calc, k_calc, b_par);
//            PrintIntegrals(j_integrals_values, j_integrals_ms);

        std::stringstream file_name;
        file_name << "result_J_Lambda_b2_" << t_beta << "_" << m_max << "_" << m_calc << "_" << k_calc << ".txt";
        std::string f_name = file_name.str();
        WriteIntegralsToFile(f_name, j_integrals_values, j_integrals_ms, m_calc, k_calc, b_par.eps_rel_ratio,
                             b_par.eps_abs_ratio, b_par.key_ratio);

    }

    return 0;
}

int calculate_ab2_clu(int argc, char** argv) {

    CalculationParameters par;
    par.t_beta = 1.;
    par.m_max = 5;
    par.index_start = 0;
    par.index_end = pow(2 * par.m_max + 1, 2);
    par.key_ratio[0] = GSL_INTEG_GAUSS15;
    par.key_ratio[1] = GSL_INTEG_GAUSS21;
    par.key_ratio[2] = GSL_INTEG_GAUSS31;
    par.key_ratio[3] = GSL_INTEG_GAUSS61;

    par.eps_abs_ratio[0] = 1e-1;
    par.eps_abs_ratio[1] = 1e-2;
    par.eps_abs_ratio[2] = 1e-3;
    par.eps_abs_ratio[3] = 1e-4;

    par.eps_rel_ratio[0] = 1e-1;
    par.eps_rel_ratio[1] = 1e-2;
    par.eps_rel_ratio[2] = 1e-3;
    par.eps_rel_ratio[3] = 1e-4;


    int calculation_type = 0; // 0 - a2, 1 - b2;
    par.calculation_type = calculation_type;

    if (argc == 1) {
        std::cout << "No input parameters" << std::endl;
        std::cout << "calculation type: 0 - a2, 1 - b2" << std::endl;
        std::cout << "t_beta" << std::endl;
        std::cout << "m_max; index_start; index_end;" << std::endl;
        std::cout << "key[4];\n"
                     "  {\n"
                     "    GSL_INTEG_GAUSS15 = 1,      /* 15 point Gauss-Kronrod rule */\n"
                     "    GSL_INTEG_GAUSS21 = 2,      /* 21 point Gauss-Kronrod rule */\n"
                     "    GSL_INTEG_GAUSS31 = 3,      /* 31 point Gauss-Kronrod rule */\n"
                     "    GSL_INTEG_GAUSS41 = 4,      /* 41 point Gauss-Kronrod rule */\n"
                     "    GSL_INTEG_GAUSS51 = 5,      /* 51 point Gauss-Kronrod rule */\n"
                     "    GSL_INTEG_GAUSS61 = 6       /* 61 point Gauss-Kronrod rule */\n"
                     "  };" << std::endl;
        std::cout << "eps_abs[4]" << std::endl;
        std::cout << "eps_rel[4]" << std::endl;
        return 1;
    } else if (argc < 6) {
        std::cout << "Not enough input parameters" << std::endl;
        return 2;
    } else {
        if (argc >= 6) {
            // TODO: check strtol insted atoi
            calculation_type = atoi(argv[1]);

            if (calculation_type != 0 && calculation_type != 1
            && calculation_type != 12 && calculation_type != 102
            && calculation_type != 104 && calculation_type != 105
            && calculation_type != 1000) {
                std::cout << "Wrong calculation type. 0 - a2 (for mu), 1 - b2, 12 - b2_mu, "
                             "102 - a2_sum, 104 - a2_in_term, 105 - a2_out_term, 1000 - a2_pure" << std::endl;
                return 3;
            }
            par.calculation_type = calculation_type;

            par.t_beta = atof(argv[2]);
            par.m_max = atoi(argv[3]);
            par.index_start = atoi(argv[4]);
            par.index_end = atoi(argv[5]);
        }
        if (argc >= 10) {
            par.key_ratio[0] = atoi(argv[6]);
            par.key_ratio[1] = atoi(argv[7]);
            par.key_ratio[2] = atoi(argv[8]);
            par.key_ratio[3] = atoi(argv[9]);
        }
        if (argc >= 14) {
            par.eps_abs_ratio[0] = atof(argv[10]);
            par.eps_abs_ratio[1] = atof(argv[11]);
            par.eps_abs_ratio[2] = atof(argv[12]);
            par.eps_abs_ratio[3] = atof(argv[13]);
            par.eps_rel_ratio[0] = atof(argv[10]);
            par.eps_rel_ratio[1] = atof(argv[11]);
            par.eps_rel_ratio[2] = atof(argv[12]);
            par.eps_rel_ratio[3] = atof(argv[13]);
        }
        if (argc >= 18) {
            par.eps_rel_ratio[0] = atof(argv[14]);
            par.eps_rel_ratio[1] = atof(argv[15]);
            par.eps_rel_ratio[2] = atof(argv[16]);
            par.eps_rel_ratio[3] = atof(argv[17]);
        }
        if (argc >= 24) {
            par.m[0] = atoi(argv[18]);
            par.m[1] = atoi(argv[19]);
            par.m[2] = atoi(argv[20]);
            par.m[3] = atoi(argv[21]);
            par.m[4] = atoi(argv[22]);
            par.m[5] = atoi(argv[23]);
        }
    }



    if (calculation_type == 0 || calculation_type == 102 || calculation_type == 104 || calculation_type == 105 || calculation_type == 1000) {

        A2SincParameters a2_par;
        a2_par.t_beta = par.t_beta;
        a2_par.n_integral = pow(2, 13);
        if (a2_par.n_integral != 0) {
            par.g2_m = GetG2Grid(a2_par);
            par.n_integral = a2_par.n_integral;
        } else {
            par.g2_m = NULL;
        }


        if (calculation_type == 0) {
            calculate_a2_for_mu(par);
        } else if (calculation_type == 102) {
            calculate_a2_sum(par);
        } else if (calculation_type == 104 || calculation_type == 105) {
            calculate_a2(par);
        } else if (calculation_type == 1000) {
            calculate_a2_pure(par);
        }

        if (par.g2_m != NULL)
            delete par.g2_m;

    } else if (calculation_type == 1 || calculation_type == 12) {

        B2SincParameters b2_par;
        b2_par.t_beta = par.t_beta;
        b2_par.n_integral = pow(2, 13);
        if (b2_par.n_integral != 0) {
            par.g3_m = GetG3Grid(b2_par);
            par.n_integral = b2_par.n_integral;
        } else {
            par.g3_m = NULL;
        }

        if (calculation_type == 1) {
            calculate_b2_wo_sum(par);
        } else if (calculation_type == 12) {
            calculate_b2_wo_sum_for_mu(par);
        }

        if (par.g3_m != NULL)
            delete par.g3_m;

    } else {

    }

    return 0;
}

int calculate_b2_four_index(int argc, char** argv) {

    int index_start = 0;
    int index_end = 1;

    if (argc == 1) {
        std::cout << "No input parameters" << std::endl;
        return 1;
    } else if (argc < 3) {
        std::cout << "No enough parameters" << std::endl;
        return 2;
    } else if (argc >= 3) {
        index_start = atoi(argv[1]);
        index_end = atoi(argv[2]);
    }

    double t_beta = 1.;
    int m_max = 5;
    double eps[4] = {1e-2, 1e-2, 1e-2, 1e-2};
    int key[4] = {GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS61};
    int grid_side = pow(2, 13);

    std::ofstream log_file;
    log_file.open("log.txt", std::ios::out | std::ios::app);
    log_file << "-------------------------------------" << std::endl;
    log_file << "[B2 4 index] [ " << index_start << " | " << index_end << " ]" << std::endl;
    log_file << "t_beta = " << t_beta << "; m_max = " << m_max << std::endl;
    log_file << "eps: " << eps[0] << " " << eps[1] << " " << eps[2] << " " << eps[3] << std::endl;
    log_file << "key: " << key[0] << " " << key[1] << " " << key[2] << " " << key[3] << std::endl;
    log_file << "grid side: " << grid_side << std::endl;
    log_file << "-------------------------------------" << std::endl;
    log_file.close();

    auto grid_point_one = std::chrono::steady_clock::now();

    B2SincParameters b2_par;
    b2_par.t_beta = t_beta;
    b2_par.n_integral = grid_side;
    if (grid_side != 0) {
        b2_par.g3_m = GetG3Grid(b2_par);
    } else {
        b2_par.g3_m = NULL;
    }

    auto grid_point_two = std::chrono::steady_clock::now();
    auto diff_ms_grid = std::chrono::duration_cast<std::chrono::milliseconds>(grid_point_two - grid_point_one);
    std::cout << "Grid worktime = " << (int)diff_ms_grid.count() << " ms" << std::endl;


    CalculationParameters par{b2_par.t_beta, m_max, index_start, index_end,
                              {key[0], key[1], key[2], key[3]},
                              {eps[0], eps[1], eps[2], eps[3]},
                              {eps[0], eps[1], eps[2], eps[3]},
                              NULL, b2_par.g3_m, b2_par.n_integral, 1, 0
    };
    calculate_b2_wo_sum(par);

    delete b2_par.g3_m;


    return 0;
}

int test_check_b2() {
    // ---* Check b2 calc

    auto grid_point_one = std::chrono::steady_clock::now();
    B2SincParameters g3_check;
    g3_check.t_beta = 1.;
    g3_check.n_integral = pow(2, 14);
    auto g3_m_check = GetG3Grid(g3_check);
    g3_check.g3_m = g3_m_check;
//    std::vector<std::complex<double>>* g3_m_check = NULL;
    auto grid_point_two = std::chrono::steady_clock::now();
    auto diff_ms_grid = std::chrono::duration_cast<std::chrono::milliseconds>(grid_point_two - grid_point_one);
    std::cout << "Grid worktime = " << (int)diff_ms_grid.count() << " ms" << std::endl;
//
//    // Seed with a real random value, if available
//    std::random_device r;
//
//    // Choose a random mean between 1 and 6
//    std::default_random_engine e1(r());
//    std::uniform_real_distribution<double> uniform_dist(-1., 1.);
////    double mean = uniform_dist(e1);
//
//    B2SincParameters g3_check_null;
//    g3_check_null.t_beta = 1.;
//    g3_check_null.n_integral = pow(2, 10);
//    g3_check_null.g3_m = NULL;
//    std::cout << std::setprecision(12);
//    for (int i = 0; i < 10; ++i) {
//        double a = uniform_dist(e1);
//        double b = uniform_dist(e1);;
//        std::complex<double> approx_val = GetG3(a, b, g3_check);
//        std::complex<double> real_val = GetG3(a, b, g3_check_null);
//        std::cout << approx_val << " " << real_val << " " << abs(approx_val - real_val) << std::endl;
//    }
//
//    std::cout << "---------------" << std::endl;
//    std::uniform_int_distribution<int> int_uniform_dist(0, pow(2,20));
//    for (int i = 0; i < 100; ++i) {
//        int pos = int_uniform_dist(e1);
//        std::cout << g3_check.g3_m->at(pos) << " " << g3_check.g3_m->at(pos + 1) << " " << abs(g3_check.g3_m->at(pos) - g3_check.g3_m->at(pos + 1)) << std::endl;
//    }

    CalculationParameters par_5{g3_check.t_beta, 15, 500, 961,
                              {GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS61},
                              {1e-2, 1e-2, 1e-2, 1e-2},
                              {1e-2, 1e-2, 1e-2, 1e-2},
                              NULL,g3_m_check, g3_check.n_integral, 1, 0
    };
    calculate_b2_wo_sum_for_mu(par_5);

    delete g3_m_check;

//    auto test_k = CalculateIndex(960, 15, 31, 2);
//    std::cout << test_k[0] << " " << test_k[1] << std::endl;

    // ---*
    return 0;
}

int test_check_a2() {
    // ---* Check a2 calc

//    auto grid_point_one = std::chrono::steady_clock::now();
//    A2SincParameters g2_check;
//    g2_check.t_beta = 1.;
//    g2_check.n_integral = pow(2, 13);
//    auto g2_m_check = GetG2Grid(g2_check);
//    g2_check.g2_m = g2_m_check;
////    std::vector<std::complex<double>>* g3_m_check = NULL;
//    auto grid_point_two = std::chrono::steady_clock::now();
//    auto diff_ms_grid = std::chrono::duration_cast<std::chrono::milliseconds>(grid_point_two - grid_point_one);
//    std::cout << "Grid worktime = " << (int)diff_ms_grid.count() << " ms" << std::endl;

    double acc = 1e-1;
//    CalculationParameters par_5{g2_check.t_beta, 5, 0, 1,
//                                {GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS15},
//                                {acc, acc, acc, acc},
//                                {acc, acc, acc, acc},
//                                g2_m_check, NULL
//    };
    CalculationParameters par_5{31.6, 15, 0, 1,
                                {GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS15},
                                {0, 0, 0, 0},
                                {acc, acc, acc, acc*0.1},
                                NULL, NULL, (int)pow(2, 13)
    };
    calculate_a2_for_mu(par_5);

//    delete g2_m_check;

//    auto test_k = CalculateIndex(960, 15, 31, 2);
//    std::cout << test_k[0] << " " << test_k[1] << std::endl;

    // ---*
    return 0;
}

int test_grid() {

    // Seed with a real random value, if available
    std::random_device r;

    // Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::uniform_real_distribution<double> uniform_dist(-1., 1.);
//    double mean = uniform_dist(e1);

//    double delta = 2. / pow(2., 14);
//
//    B2SincParameters g3_check_null;
//    g3_check_null.t_beta = 1.;
//    g3_check_null.n_integral = pow(2, 14);
//    g3_check_null.g3_m = NULL;
////    auto g3_m_check = GetG3Grid(g3_check);
//    std::cout << std::setprecision(12);
//    for (int i = 0; i < 100; ++i) {
//        double a = uniform_dist(e1);
//        double b = uniform_dist(e1);;
//        std::complex<double> approx_val = GetG3(a + delta, b, g3_check_null);
//        std::complex<double> real_val = GetG3(a, b, g3_check_null);
//        std::cout << approx_val << " " << real_val << " " << abs(approx_val - real_val) << std::endl;
//    }


    A2SincParameters g2_check;
    A2SincParameters g2_check_null;
    g2_check.t_beta = 31.6;
    g2_check_null.t_beta = g2_check.t_beta;
    g2_check.n_integral = pow(2, 10);
    g2_check_null.n_integral = g2_check.n_integral;

    auto time_point_one = std::chrono::steady_clock::now();

    g2_check.g2_m = GetG2Grid(g2_check);

    auto time_point_two = std::chrono::steady_clock::now();
    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
    std::cout << "Grid calc = " << (int)diff_ms.count() << " ms" << std::endl;

    std::cout << std::setprecision(12);
    for (int i = 0; i < 10; ++i) {
        double a = uniform_dist(e1);
        double b = uniform_dist(e1);;
        std::complex<double> approx_val = GetG2(a, b, g2_check);
        std::complex<double> real_val = GetG2(a, b, g2_check_null);
        std::cout << approx_val << " " << real_val << " " << abs(approx_val - real_val) << std::endl;
    }

//    time_point_one = std::chrono::steady_clock::now();
//
//    for (int i = 0; i < pow(2,28); ++i) {
//        double a = uniform_dist(e1);
//        double b = uniform_dist(e1);
//        std::complex<double> approx_val = GetG2(a, b, g2_check);
//    }
//
//    time_point_two = std::chrono::steady_clock::now();
//    diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
//    std::cout << "With grid calc = " << (int)diff_ms.count() << " ms" << std::endl;
//
//    time_point_one = std::chrono::steady_clock::now();
//
//    for (int i = 0; i < pow(2,28); ++i) {
//        double a = uniform_dist(e1);
//        double b = uniform_dist(e1);
//        std::complex<double> real_val = GetG2(a, b, g2_check_null);
//    }
//
//    time_point_two = std::chrono::steady_clock::now();
//    diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
//    std::cout << "Without grid calc = " << (int)diff_ms.count() << " ms" << std::endl;


    auto temp_p = g2_check.g2_m;
    g2_check.g2_m = NULL;
    delete temp_p;

    return 0;
}

int test_math_functions() {

    int N = pow(2,22);
    // Seed with a real random value, if available
    std::random_device r;

    // Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::uniform_real_distribution<double> uniform_dist(0, 2. * M_PI);
//    double mean = uniform_dist(e1);

    auto time_point_one = std::chrono::steady_clock::now();

    gsl_complex sum_gsl = gsl_complex_rect(0, 0);
    for (int i = 0; i < N; ++i) {
        double x = uniform_dist(e1);
        double y = uniform_dist(e1);
//        auto a = gsl_sf_sin(x);
        auto a = gsl_complex_exp(gsl_complex_rect(x, y));
        gsl_complex_add(sum_gsl, a);
//        std::cout << a << " ";
    }
    std::cout << GSL_REAL(sum_gsl) << " " << GSL_IMAG(sum_gsl) << std::endl;
    std::cout << std::endl;

    auto time_point_two = std::chrono::steady_clock::now();
    auto diff_ms = std::chrono::duration_cast<std::chrono::microseconds>(time_point_two - time_point_one);
    std::cout << "GSL sin = " << (int)diff_ms.count() << " ms" << std::endl;


    std::cout << "Now is our champion!" << std::endl;
    time_point_one = std::chrono::steady_clock::now();

    std::complex<double> sum = 0;
    for (int i = 0; i < N; ++i) {
        double x = uniform_dist(e1);
        double y = uniform_dist(e1);
//        auto a = sin(x);
        auto a = exp(std::complex<double>(x, y));
        sum += a;
//        std::cout << a << " ";
    }
    std::cout << sum << std::endl;
    std::cout << std::endl;

    time_point_two = std::chrono::steady_clock::now();
    diff_ms = std::chrono::duration_cast<std::chrono::microseconds>(time_point_two - time_point_one);
    std::cout << "Usual sin = " << (int)diff_ms.count() << " ms" << std::endl;

    return 0;
}

void signal_handler(int signum) {
    std::cout << "Interrupt signal (" << signum << ") received.\n";
    std::cout << "Actually we can not to interupt calculations" << std::endl;
    // cleanup and close up stuff here
    // terminate program

//    exit(signum);
}


int test_get_index() {

    const int k_max_value = 5;
    const int k_var_value = 2 * k_max_value + 1;
    const int k_total = pow(k_var_value * (k_max_value + 1), 2);
    for (int i = 0; i < k_total; ++i) {

        //        auto k = CalculateIndex(i, k_max_value, k_var_value, 4);
        auto k = CalculateHalfIndex(i, k_max_value, 4);

        std::cout << "[" << k[0] << "][" << k[1] << "][" << k[2] << "][" << k[3] << "]" << std::endl;
    }


    return 0;
}

int test_sum_exp() {

    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_real_distribution<double> uniform_dist(-100., 100.);

    for (int m = 5; m < 17; m += 2) {
        for (int i = 0; i < 100; ++i) {
            double a = uniform_dist(e1);
            std::complex<double> sum = GetSumExp(a, m);
            if (abs(sum.imag()) > 1e-14) {
                std::cout << "GetSumExp error: sum imag part != 0 -> " << sum.imag() << std::endl;
            }

            std::complex<double> check_sum = 0.;
            for (int k = -m; k <= m; ++k) {
                check_sum += Get2piExp(a * k);
            }

            if (abs(check_sum.imag()) > 1e-14) {
                std::cout << "GetSumExp error: check sum imag part != 0 -> " << check_sum.imag() << std::endl;
            }

            if (abs(check_sum.real() - sum.real()) > 1e-8) {
                std::cout << "GetSumExp error: sum != check sum -> " << sum.real() << " " << check_sum.real()
                << " -> |sum - check_sum| = " << abs(check_sum.real() - sum.real()) << std::endl;
            }
        }
    }

    return 0;
}

int test_2_exp() {

    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_real_distribution<double> uniform_dist(-100., 100.);
    std::uniform_int_distribution<> uniform_int(-15., 15.);


    for (int i = 0; i < 100000; ++i) {


        int m = uniform_int(e1);
        int p = uniform_int(e1);

        double k1 = uniform_dist(e1);
        double k2 = uniform_dist(e1);

        auto sum_exp = exp(2. * M_PI * 1i * (m * k1 + p * k2)) + exp(2. * M_PI * 1i * (p * k1 + m * k2));

        auto sum_exp_other_form = 2. * exp(M_PI * 1i * ((m + p) * (k1 + k2))) * cos(M_PI * (m - p) * (k1 - k2));

        if (abs(sum_exp - sum_exp_other_form) > 1e-11) {
            std::cout << abs(sum_exp - sum_exp_other_form) << std::endl;
        }

    }

    return 0;
}

int run_a2_pure_1(int s1, int s2, int s3, int s4, int m, int argc, char** argv) {

    for (int i = -m; i <= m; i++) {
//        std::cout << argv[18] << " " << argv[19] << " " << argv[20] << " "
//                << argv[21] << " " << argv[22] << " " << argv[23] << std::endl;

        char str_s1[20];
        char str_s2[20];
        char str_s3[20];
        char str_s4[20];
        char str_i[20];
        sprintf(str_s1, "%d", s1);
        sprintf(str_s2, "%d", s2);
        sprintf(str_s3, "%d", s3);
        sprintf(str_s4, "%d", s4);
        sprintf(str_i, "%d", i);

        argv[18] = str_s1;
        argv[19] = str_s2;
        argv[20] = str_i;
        argv[21] = str_i;
        argv[22] = str_s3;
        argv[23] = str_s4;

//        std::cout << argv[18] << " " << argv[19] << " " << argv[20] << " "
//                  << argv[21] << " " << argv[22] << " " << argv[23] << std::endl;


        auto time_point_one = std::chrono::steady_clock::now();
        calculate_ab2_clu(argc, argv);
        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::cout << argv[18] << " " << argv[19] << " " << argv[20] << " "
                  << argv[21] << " " << argv[22] << " " << argv[23] << std::endl;

        std::cout << "Calculation time = " << (int)diff_ms.count() << " ms" << std::endl;

        std::cout << "----------" << std::endl;
    }

    return 0;
}

int run_a2_pure_2(int s1, int s2, int s3, int s4, int m, int argc, char** argv) {

    for (int i = -m; i <= m; i++) {
//        std::cout << argv[18] << " " << argv[19] << " " << argv[20] << " "
//                << argv[21] << " " << argv[22] << " " << argv[23] << std::endl;

        char str_s1[20];
        char str_s2[20];
        char str_s3[20];
        char str_s4[20];
        char str_i[20];
        sprintf(str_s1, "%d", s1);
        sprintf(str_s2, "%d", s2);
        sprintf(str_s3, "%d", s3);
        sprintf(str_s4, "%d", s4);
        sprintf(str_i, "%d", i);

        argv[18] = str_i;
        argv[19] = str_s1;
        argv[20] = str_s2;
        argv[21] = str_s3;
        argv[22] = str_s4;
        argv[23] = str_i;

//        std::cout << argv[18] << " " << argv[19] << " " << argv[20] << " "
//                  << argv[21] << " " << argv[22] << " " << argv[23] << std::endl;


        auto time_point_one = std::chrono::steady_clock::now();
        calculate_ab2_clu(argc, argv);
        auto time_point_two = std::chrono::steady_clock::now();
        auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

        std::cout << argv[18] << " " << argv[19] << " " << argv[20] << " "
                  << argv[21] << " " << argv[22] << " " << argv[23] << std::endl;

        std::cout << "Calculation time = " << (int)diff_ms.count() << " ms" << std::endl;

        std::cout << "----------" << std::endl;
    }

    return 0;
}

int run_a2_pure_all(int s1, int s2, int s3, int s4, int m, int argc, char** argv) {

    run_a2_pure_1(s1, s2, s3, s4, m, argc, argv);
    run_a2_pure_2(s1, s2, s3, s4, m, argc, argv);

    return 0;
}

int run_a2_pure_q(int q, int m, int argc, char** argv) {

    run_a2_pure_all(q, q, q, q, m, argc, argv);

    return 0;
}

int run_a2_pure_ij(int i, int j, int m, int argc, char** argv) {

    run_a2_pure_all(i, i, j, j, m, argc, argv);
    run_a2_pure_all(i, j, i, j, m, argc, argv);
    run_a2_pure_all(i, j, j, i, m, argc, argv);
    run_a2_pure_all(j, i, j, i, m, argc, argv);
    run_a2_pure_all(i, i, i, j, m, argc, argv);
    run_a2_pure_all(i, i, j, i, m, argc, argv);
    run_a2_pure_all(i, j, j, j, m, argc, argv);
    run_a2_pure_all(j, i, j, j, m, argc, argv);

    return 0;
}

int main(int argc, char** argv) {

    auto time_point_one = std::chrono::steady_clock::now();

//    calculate_int_clu(argc, argv);
//    calculate_int_by_sum(argc, argv);
//    check_calculations(argc, argv);
//    test_new_interface(argc, argv);
//    test_ab1_integrals();
//    calculate_ab1(0.0);
//    calculate_ab1(1.0);
//    calculate_ab1(31.6);
//    calculate_ab1(5.0);
//    calculate_ab1(10.0);
//    test_b_5d();
    test_a2_5d();

//    try {
//        calculate_b2(1., 0);
//    } catch (const std::exception& e) {
//        std::cout << e.what() << std::endl;
//    };

//    signal(SIGINT, signal_handler);
//    signal(SIGABRT, signal_handler);

//    CalculationParameters par_5{1., 5, 0, (2 * 5 + 1) * (5 + 1),
//                              {GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS21, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS61},
//                              {1e-1, 1e-2, 1e-3, 1e-4},
//                              {1e-1, 1e-2, 1e-3, 1e-4}
//    };
//
//    CalculationParameters par_10{1., 10, 0, (2 * 10 + 1) * (10 + 1),
//                                {GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS21, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS61},
//                                {1e-1, 1e-2, 1e-3, 1e-4},
//                                {1e-1, 1e-2, 1e-3, 1e-4}
//    };
//
//    CalculationParameters par_15{1., 15, 0, (2 * 15 + 1) * (15 + 1),
//                                 {GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS21, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS61},
//                                 {1e-1, 1e-2, 1e-3, 1e-4},
//                                 {1e-1, 1e-2, 1e-3, 1e-4}
//    };
//
//    calculate_a2(par_5);
//    calculate_a2(par_10);
//    calculate_a2(par_15);


//    check_calculations(argc, argv);

//    for (int i = 0; i < 441; ++i) {
//        auto test_k = CalculateIndex(i, 10, 21, 2);
//        std::cout << i << " " << test_k[0] << " " << test_k[1] << std::endl;
//    }

//    test_check_a2();
//    test_check_b2();
//    test_grid();
//    test_math_functions();
//    test_get_index();
//    test_sum_exp();
//    test_2_exp();

    // ---- Check how much time we need to calculate G2 and G3

//    A2SincParameters g2_check;
//    g2_check.n_integral = pow(2, 13);
//    auto g2_m_check = GetG2Grid(g2_check);
//    delete g2_m_check;

//    B2SincParameters g3_check;
//    g3_check.n_integral = pow(2, 13);
//    auto g3_m_check = GetG3Grid(g3_check);
//    delete g3_m_check;

    // ---- End check

//    calculate_b2_four_index(argc, argv);
//    calculate_ab2_clu(argc, argv);

//    run_a2_pure_q(0, 5, argc, argv);
//    run_a2_pure_ij(0, 1, 5, argc, argv);

    auto time_point_two = std::chrono::steady_clock::now();
    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
    std::cout << "Total worktime = " << (int)diff_ms.count() << " ms" << std::endl;

//    std::cout << test_return() << std::endl;

    return 0;
}
