//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_INTEGRALS_H
#define OPTIMALCHANNELINT_INTEGRALS_H

#include <complex>
#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <iomanip>
#include <fstream>

#include "gsl/gsl_integration.h"

#ifndef A2_SINC_PARAMETERS
#define A2_SINC_PARAMETERS

struct A2SincParameters {
    double t_beta = 31.6;
    int m[6]; // m[0] <-> m1, m[1] <-> m2 and so on
    int N;
    int j_k;
    int j_m;

    int M = 5;
    int sum_key = 0; // 0 - without sum, 1 - sum type 1 (m2 and m4), 2 - sum type 2 (m2 and m5), 3 - J (A2 term) calc

    double eps_abs = 1e-4;
    double eps_rel = 1e-4;
    int key = GSL_INTEG_GAUSS61;
    size_t n_intervals = 10000000;

    double eps_abs_ratio[4] = {1e-4, 1e-4, 1e-4, 1e-4};
    double eps_rel_ratio[4] = {1e-4, 1e-4, 1e-4, 1e-4};
    int key_ratio[4] = {GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61};

    double a_start = -1.0;
    double a_end = 1.0;
    double b_start = -1.0;
    double b_end = 1.0;
    int n_integral = pow(2, 13);
    std::vector<std::complex<double>>* g2_m = NULL;

    int n_fraction = 1;
    int ind_fraction = 0;

    double t_int_bot = 0.;
    double t_int_up = 1.;
    double z_int_bot = -M_PI_2;
    double z_int_up = M_PI_2;
};


#endif // A2_SINC_PARAMETERS

#ifndef B2_SINC_PARAMETERS
#define B2_SINC_PARAMETERS

struct B2SincParameters {
    double t_beta = 31.6;
    int k[4]; // k[0] <-> k1, k[1] <-> k2 and so on
    int N; // N = k1 + k2 - k3 - k4
    int j_k;
    int j_m;

    int M = 5;
    int sum_key = 0; // 0 - without any sum, 1 - not used, 2 - not used, 3 - J_Lambda (B2 term) calc

    double eps_abs = 1e-4;
    double eps_rel = 1e-4;
    int key = GSL_INTEG_GAUSS61;
    size_t n_intervals = 10000000;

    double eps_abs_ratio[4] = {1e-4, 1e-4, 1e-4, 1e-4};
    double eps_rel_ratio[4] = {1e-4, 1e-4, 1e-4, 1e-4};
    int key_ratio[4] = {GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61};

    double a_int_bot = -100.;
    double a_int_up = 100.;
    double z_int_bot = -20.;
    double z_int_up = 20.;

    double a_start = -1.0;
    double a_end = 1.0;
    double b_start = -1.0;
    double b_end = 1.0;
    int n_integral = pow(2, 13);
    std::vector<std::complex<double>>* g3_m = NULL;

    int n_fraction = 1;
    int ind_fraction = 0;
};


#endif // B2_SINC_PARAMETERS

#ifndef CALCULATION_PARAMETERS
#define CALCULATION_PARAMETERS

struct CalculationParameters {
    double t_beta = 1.;
    int m_max = 5;
    int index_start = 0;
    int index_end = 1;


    int key_ratio[4] = {GSL_INTEG_GAUSS15, GSL_INTEG_GAUSS21, GSL_INTEG_GAUSS31, GSL_INTEG_GAUSS61};
    double eps_abs_ratio[4] = {1e-1, 1e-2, 1e-3, 1e-4};
    double eps_rel_ratio[4] = {1e-1, 1e-2, 1e-3, 1e-4};
//    int key_ratio[4];
//    double eps_abs_ratio[4];
//    double eps_rel_ratio[4];


    std::vector<std::complex<double>>* g2_m = NULL;
    std::vector<std::complex<double>>* g3_m = NULL;
    int n_integral = pow(2, 13);

    int n_fraction = 1;
    int ind_fraction = 0;

    int calculation_type = 0;
    int m[6] = {0, 0, 0, 0, 0, 0};
};

#endif // CALCULATION_PARAMETERS

#include "include/a2sinc_integral1.h"
#include "include/a2sinc_integral2.h"
#include "include/a2sinc_integral3.h"
#include "include/a2sinc_integral4.h"
#include "include/a2sinc_integral5.h"
#include "include/a2sinc_integral6.h"
#include "include/a2sinc_integral7.h"
#include "include/a2sinc_integral8.h"
#include "include/a2sinc_integral9.h"
#include "include/a2sinc_integral10.h"
#include "include/a2sinc_integral11.h"

#include "include/a2sinc_check_integral_5d.h"
#include "include/b2sinc_check_integral_5d.h"

#include "include/b2sinc_integral1.h"
#include "include/b2sinc_integral2.h"
#include "include/b2sinc_integral3.h"
#include "include/b2sinc_integral4.h"
#include "include/b2sinc_integral5.h"
#include "include/b2sinc_integral6.h"
#include "include/b2sinc_integral7.h"
#include "include/b2sinc_integral9.h"
#include "include/b2sinc_integral11.h"

using namespace std::complex_literals; // TODO: it is for cluster

// Calculate A2^{m1, m2, m3; m4, m5, m6} for sinc function

// Headers
//void PrintA2SincParameters (A2SincParameters par) {
//    std::cout << "t_beta = " << par.t_beta << std::endl;
//
//    std::cout << "m = ";
//    for (int i = 0; i < 6; ++i) {
//        std::cout << par.m[i] << " "; 
//    }
//    std::cout << std::endl;
//
//    std::cout << "eps_abs = " << par.eps_abs << std::endl;
//    std::cout << "eps_rel = " << par.eps_rel << std::endl;
//    std::cout << "key = " << par.key << std::endl;
//    std::cout << "n_intervals = " << par.n_intervals << std::endl;
//    std::cout << "a_start = " << par.a_start << std::endl;
//    std::cout << "a_end = " << par.a_end << std::endl;
//    std::cout << "b_start = " << par.b_start << std::endl;
//    std::cout << "b_end = " << par.b_end << std::endl;
//    std::cout << "n_integral = " << par.n_integral << std::endl;
//    std::cout << "g2_m = " << par.g2_m << std::endl;
//    
//}

void swap(double* a, double* b, double* sign);

std::complex<double> Sinc(std::complex<double> x);

inline int factorial(int n)
{
    return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

//int GetN(A2SincParameters par);
inline int GetN(A2SincParameters par) {
    int N = par.m[0] + par.m[1] + par.m[2] - par.m[3] - par.m[4] - par.m[5];
    return N;
}

inline int GetN(B2SincParameters par) {
    int N = par.k[0] + par.k[1] - par.k[2] - par.k[3];
    return N;
}

inline std::complex<double> GetG(double x) {
    double accuracy = 1e-6;
    std::complex<double> im = 1i;
    if (x < accuracy) {
        std::complex<double> sum = 0.;
        for (int i = 0; i < 5; ++i) {
            sum += -im * std::pow(-im * x, i) / ((i + 1) * (double)factorial(i));
        }
        return sum;
    } else {
        return std::complex<double>((cos(x) - 1) / x, -sin(x) / x);
    }
}

inline std::complex<double> GetG1(double x) {
    double accuracy = 1e-6;
    std::complex<double> im = 1i;
    if (x < accuracy) {
        std::complex<double> sum = 0.;
        for (int i = 0; i < 5; ++i) {
            sum += -im * std::pow(-im * x, i) / ((i + 2) * (double)factorial(i));
        }
        return sum;
    } else {
        return std::complex<double>((x * cos(x) - sin(x)) / pow(x, 2), (1 - cos(x) - x * sin(x)) / pow(x, 2));
    }
}

//std::complex<double> GetG2(double a, double b, A2SincParameters par);
inline std::complex<double> GetG2(double a, double b, A2SincParameters par) {

//    if (abs(a) > 1. || abs(b) > 1.) {
//        std::cout << "[GetG3]: |a| or |b| > 1" << std::endl;
//    }

    if (par.g2_m != NULL) {
        double a_pos = (a - par.a_start) / (par.a_end - par.a_start) * (double)par.n_integral;
        double b_pos = (b - par.b_start) / (par.b_end - par.b_start) * (double)par.n_integral;
        int a_floor = floor(a_pos), a_ceil = ceil(a_pos); // Array index
        int b_floor = floor(b_pos), b_ceil = ceil(b_pos);

        double a_part = a_pos - (double)a_floor; // 0 - a_floor, 1 - a_ceil
        double b_part = b_pos - (double)b_floor;

        std::complex<double> result = 0.;
        result = ( par.g2_m->at(a_floor * (par.n_integral + 1) + b_floor) * (1. - a_part) +
                  par.g2_m->at(a_ceil * (par.n_integral + 1) + b_floor) * a_part ) * (1. - b_part) +
                 ( par.g2_m->at(a_floor * (par.n_integral + 1) + b_ceil) * (1. - a_part) +
                  par.g2_m->at(a_ceil * (par.n_integral + 1) + b_ceil) * a_part ) * b_part;
        return result;
    }

    if (par.t_beta == 0)
        return 0.5;

    std::complex<double> im = 1i;
    std::complex<double> k = 4. * im * par.t_beta;
    std::complex<double> result = 0.;
    std::complex<double> ka = k * a;
    std::complex<double> kb = k * b;
    std::complex<double> ks = k * (a + b);
    double tolerance = 1e-6;

    if (abs(a) < tolerance) {
        if (abs(b) < tolerance) {
            result = (0.5 + k * a / 3. + pow(k * a, 2) / 8. + pow(k * a, 3) / 30.) + (k / 6. + pow(k, 2) * a / 8. + pow(k * a, 2) * k / 20. + pow(k, 3) * a * b / 30.) * b + pow(k * b, 3) / 120.;
        } else {
            result = -1. / (16. * pow(par.t_beta, 2) * b) *
                     ((exp(k * (a + b)) - 1.) / (a + b) - k * (1. + ka * 0.5 + pow(ka, 2) / 6. + pow(ka, 3) / 24.));
        }
    } else if (abs(b) < tolerance) {
        //std::complex<double> exp_ka = exp(k * a);
        //std::complex<double> k_m2 = pow(k, -2);
        //result = k_m2 * pow(a, -2) * (1. + (k * a - 1.) * exp_ka ) + b * k_m2 * pow(a, -3) * 0.5 * (-2. + exp_ka * (2. - 2. * k * a + pow(k * a, 2))) +
        //         pow(b, 2) * k_m2 * pow(a, -4) / 6. * (6. + exp_ka * (-6. + 6. * k * a - 3. * pow(k * a, 2) + pow(k * a, 3))) +
        //         pow(b, 3) * k_m2 * pow(a, -5) / 24. * (-24. + exp_ka * (24. - 24. * k * a + 12. * pow(k * a, 2) - 4. * pow(k * a, 3) + pow(k * a, 4)));
//        result = k_m2 * pow(a, -2) * (1. + k * a * exp_ka - exp_ka);

        result = -1. / (16. * pow(par.t_beta, 2) * (a + b)) *
            (exp(ka) * k * (1. + kb * 0.5 + pow(kb, 2) / 6. + pow(kb, 3) / 24.) - (exp(ka) - 1.) / a);
    } else if (abs(a + b) < tolerance) {
        result = -1. / (16. * pow(par.t_beta, 2) * b) *
            ( k * (1. + ks * 0.5 + pow(ks, 2) / 6. + pow(ks, 3) / 24.) - (exp(ka) - 1.) / a);
        
    } else {
        result = -1. / (16. * pow(par.t_beta, 2) * b) *
                 ((exp(k * (a + b)) - 1.) / (a + b) - (exp(ka) - 1.) / a);
    }

    return result;
}

inline std::complex<double> GetG3(double a, double b, B2SincParameters par) {

//    if (abs(a) > 1. || abs(b) > 1.) {
//        std::cout << "[GetG3]: |a| or |b| > 1" << std::endl;
//    }

    if (par.g3_m != NULL) {
        double a_pos = (a - par.a_start) / (par.a_end - par.a_start) * (double)par.n_integral;
        double b_pos = (b - par.b_start) / (par.b_end - par.b_start) * (double)par.n_integral;
        int a_floor = floor(a_pos), a_ceil = ceil(a_pos); // Array index
        int b_floor = floor(b_pos), b_ceil = ceil(b_pos);

        double a_part = a_pos - (double)a_floor; // 0 - a_floor, 1 - a_ceil
        double b_part = b_pos - (double)b_floor;



        std::complex<double> val_af_bf = par.g3_m->at(a_floor * (par.n_integral + 1) + b_floor);
        std::complex<double> val_ac_bf = par.g3_m->at(a_ceil * (par.n_integral + 1) + b_floor);
        std::complex<double> val_af_bc = par.g3_m->at(a_floor * (par.n_integral + 1) + b_ceil);
        std::complex<double> val_ac_bc = par.g3_m->at(a_ceil * (par.n_integral + 1) + b_ceil);

        std::complex<double> result = (val_af_bf * (1. - a_part) + val_ac_bf * a_part) * (1. - b_part) +
                (val_af_bc * (1. - a_part) + val_ac_bc * a_part) * b_part;

//        std::complex<double> result = 0.;
//        result = ( par.g3_m->at(a_floor * (par.n_integral + 1) + b_floor) * (1. - a_part) +
//                   par.g3_m->at(a_ceil * (par.n_integral + 1) + b_floor) * a_part ) * (1. - b_part) +
//                 ( par.g3_m->at(a_floor * (par.n_integral + 1) + b_ceil) * (1. - a_part) +
//                   par.g3_m->at(a_ceil * (par.n_integral + 1) + b_ceil) * a_part ) * b_part;
        return result;
    }

    if (par.t_beta == 0)
        return 1./3.;

    std::complex<double> im = 1i;
    std::complex<double> k = 4. * par.t_beta;
    std::complex<double> k_im = k * im;
    std::complex<double> result = 0.;
    std::complex<double> ka_im = k_im * a;
    std::complex<double> kb_im = k_im * b;
    std::complex<double> ks_im = k_im * (a + b);
    double tolerance = 1e-6;

    if (abs(a) < tolerance) {
        if (abs(b) < tolerance) {
            // Case number 4
            result = 1./3. + 5./24. * k * im * (a + b) - pow(k, 2.) * (3./40. * (pow(a, 2.) + pow(b, 2))+ 2./15. * a * b) -
                    7. * pow(k, 3) * im * (1./360. * (pow(a, 3.) + pow(b, 3.)) + 1./144. * a * b * (a + b));

            // k^3*(-7*I*a^3/360 - 7*I*a^2*b/144 - 7*I*a*b^2/144 - 7*I*b^3/360) + k^2*(-3*a^2/40 - 2*a*b/15 - 3*b^2/40) + k*(5*I*a/24 + 5*I*b/24) + 1/3
        } else {
            // Case number 2
            result = im / (pow(k, 3.) * pow(b, 2.) * (a + b)) *
                    ((1. + exp(kb_im) * (kb_im - 1.)) *
                    (exp(ka_im) + b * (k_im + pow(k_im, 2.) / 2. * a + pow(k_im, 3.) / 6. * pow(a, 2.) +
                    pow(k_im, 4.) / 24. * pow(a, 3.) + pow(k_im, 5.) / 120. * pow(a, 4.))) -
                    pow(b, 2.) * exp(kb_im) * (pow(k_im, 2.) / 2.  + pow(k_im, 3.) / 6. * a +
                    pow(k_im, 4.) / 24. * pow(a, 2.) + pow(k_im, 5.) / 120. * pow(a, 3.)));
        }
    } else if (abs(b) < tolerance) {
        // Case number 3
        result = GetG3(b, a, par);
    } else if (abs(a + b) < tolerance) {
        // Case number 1
        result = im / (pow(k, 3.) * pow(a * b, 2.)) *
                (exp(ks_im) * (k_im * a * b  - (a + b)) + (a * exp(ka_im) + b * exp(kb_im)) +
                a * b * (k_im + pow(k_im, 2.) / 2. * (a + b) + pow(k_im, 3.) / 6. * pow(a + b, 2.) +
                pow(k_im, 4.) / 24. * pow(a + b, 3.) + pow(k_im, 5.) / 120. * pow(a + b, 4.)));
    } else {
        // General case
        result = im / (pow(k, 3.) * pow(a * b, 2.) * (a + b)) * (exp(ks_im) * (k_im * a * b * (a + b) - pow(a + b, 2.) + a * b) + (a + b) * (a * exp(ka_im) + b * exp(kb_im)) - a * b);
    }

    return result;
}

inline std::complex<double> GetExSum(double y4, double y3, double y2, double y1, A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double s = par.m[2];
    double r = par.m[0];

    std::complex<double> sum = 0.;

    if (par.sum_key == 1) {
        result = exp(2. * M_PI * im * ((y2 - y1) * s + (y1 - y2 - y3 - y4) * r));
        std::complex<double> exp_value = exp(2. * M_PI * im * (y3 + y4));
        if (exp_value != 1.) {
            sum = (exp(-2. * M_PI * im * (y3 + y4) * (double)par.M) - exp(2. * M_PI * im * (y3 + y4) * (double)(par.M + 1))) / (1. - exp_value);
        } else {
            for (int i = -par.M; i <= par.M; ++i) {
                sum += exp(2. * M_PI * im * (y3 + y4) * (double)i);
            }
        }
    } else if (par.sum_key == 2) {
        result = exp(2. * M_PI * im * ((y2 + y3) * 2. * s + (y1 - y2 - y3 - y4) * r));
        std::complex<double> exp_value = exp(2. * M_PI * im * (y4 - y1 - y2 - y3));
        if (exp_value != 1.) {
            sum = (exp(-2. * M_PI * im * (y4 - y1 - y2 - y3) * (double)par.M) - exp(2. * M_PI * im * (y4 - y1 - y2 - y3) * (double)(par.M + 1))) / (1. - exp_value);
        } else {
            for (int i = -par.M; i <= par.M; ++i) {
                sum += exp(2. * M_PI * im * (y4 - y1 - y2 - y3) * (double)i);
            }
        }
    } else {
        std::cout << "[GetExSum]: undefined sum case" << std::endl;
    }
    result *= sum;

    return result;
}

inline std::complex<double> Get2piExp(double x) {
    std::complex<double> im = 1i;
    return exp(2. * M_PI * im * x);
}

inline std::complex<double> GetSumExp(double x, int m_max) {
    std::complex<double> result = 0.;
//    std::complex<double> exp_value = Get2piExp(x);
//    if (exp_value != 1.) {
//        result = (Get2piExp(-1. * x * (double)m_max) - Get2piExp(x * (double)(m_max + 1))) / (1. - exp_value);
//    } else {
//        for (int i = -m_max; i <= m_max; ++i) {
//            result += Get2piExp(x * (double)i);
//        }
//    }

    //  std::floor(std::abs(k)) == std::abs(k)
    double sin_pi_x = sin(M_PI * x);
    if (sin_pi_x != 0) {
        result = sin(2. * M_PI * x * ((double)m_max + 0.5)) / sin_pi_x;
    } else {
        result = 1.;
        for (int i = 1; i <= m_max; ++i) {
            result += 2. * cos(2. * M_PI * x * (double)i);
        }
    }

    return result;
}

//inline std::complex<double> TestGetExSumBy2Var(double y4, double y3, double y2, double y1, A2SincParameters par) {
//    std::complex<double> im = 1i;
//    std::complex<double> result = 0.;
//
//    double k = (double)par.j_k; // index for calculation
//    double m = (double)par.j_m;
//    double k_minus_m = k - m;
//    double k_plus_m = k + m;
//
//    // Temp variables to accelerate calculations
//    double y_comb[8];
//    y_comb[0] = -(y1 + y2 + y3 + y4);
//    y_comb[1] = y1 - y2 - y3 - y4;
//    y_comb[2] = -y1 - y2 - y3 + y4;
//    y_comb[3] = y1 - y2 - y3 + y4;
//
//    y_comb[4] = 2. * (y2 + y3);
//    y_comb[5] = 2. * y2 + y3 + y4;
//    y_comb[6] = y3 - y4;
//    y_comb[7] = 2. * y4;
//
//    std::complex<double> sum_exp_y_comb[5];
//    for (int j = 0; j < 5; ++j) {
//        sum_exp_y_comb[j] = GetSumExp(y_comb[j], par.M);
//    }
//
//    std::complex<double> first_term = 0.;
//    std::complex<double> second_term = 0.;
//
////    std::complex<double> exp_p_sum_value = GetSumExp(2. * (y2 + y3), par.M);
//    std::complex<double> exp_p_sum_value = sum_exp_y_comb[4];
//
//    std::complex<double> exp_r_sum_value[4];
////    exp_r_sum_value[0] = GetSumExp(-y1 - y2 - y3 - y4, par.M);
////    exp_r_sum_value[1] = GetSumExp(y1 - y2 - y3 - y4, par.M);
////    exp_r_sum_value[2] = GetSumExp(-y1 - y2 - y3 + y4, par.M);
////    exp_r_sum_value[3] = GetSumExp(y1 - y2 - y3 + y4, par.M);
//    for (int j = 0; j < 4; ++j) {
//        exp_r_sum_value[j] = sum_exp_y_comb[j];
//    }
//
//    if (k == m) {
//        first_term = exp_p_sum_value * (
//                exp_r_sum_value[0] * 2. * cos(M_PI * k_plus_m * (y_comb[1] + y_comb[7])) +
//                exp_r_sum_value[1] * 2. * cos(M_PI * k_plus_m * (y_comb[0] + y_comb[7])) +
//                exp_r_sum_value[2] * 2. * cos(M_PI * k_plus_m * y_comb[1]) +
//                exp_r_sum_value[3] * 2. * cos(M_PI * k_plus_m * y_comb[0])
//        );
//    } else if (k == -m) {
//        first_term = exp_p_sum_value * (
//                exp_r_sum_value[0] * 2. * exp(im * M_PI * k_minus_m * (y_comb[1] - y_comb[7])) +
//                exp_r_sum_value[1] * 2. * exp(im * M_PI * k_minus_m * (y_comb[0] - y_comb[7])) +
//                exp_r_sum_value[2] * 2. * exp(im * M_PI * k_minus_m * y_comb[1]) +
//                exp_r_sum_value[3] * 2. * exp(im * M_PI * k_minus_m * y_comb[0])
//        );
//    } else {
//        first_term = exp_p_sum_value * (
//                exp_r_sum_value[0] * 2. * exp(im * M_PI * k_minus_m * (y_comb[1] - y_comb[7])) * cos(M_PI * k_plus_m * (y_comb[1] + y_comb[7])) +
//                exp_r_sum_value[1] * 2. * exp(im * M_PI * k_minus_m * (y_comb[0] - y_comb[7])) * cos(M_PI * k_plus_m * (y_comb[0] + y_comb[7])) +
//                exp_r_sum_value[2] * 2. * exp(im * M_PI * k_minus_m * y_comb[1]) * cos(M_PI * k_plus_m * y_comb[1]) +
//                exp_r_sum_value[3] * 2. * exp(im * M_PI * k_minus_m * y_comb[0]) * cos(M_PI * k_plus_m * y_comb[0])
//        );
//    }
//
////    exp_p_sum_value = GetSumExp(y1 - y2 - y3 - y4, par.M);
//    exp_p_sum_value = sum_exp_y_comb[1];
//
//    exp_r_sum_value[0] = GetSumExp(y3 + y4, par.M);
////    exp_r_sum_value[1] = GetSumExp(-y1 - y2 - y3 + y4, par.M);
//    exp_r_sum_value[1] = sum_exp_y_comb[2];
////    exp_r_sum_value[2] = GetSumExp(2. * (y2 + y3), par.M);
//    exp_r_sum_value[2] = sum_exp_y_comb[4];
//    exp_r_sum_value[3] = GetSumExp(-y1 + y2, par.M);
//
//
////    second_term = exp_p_sum_value * (
////            exp_r_sum_value[0] * Get2piExp(-(y1 + y2 + y3 + y4) * k + (2. * y2 + y3 + y4) * m) +
////            exp_r_sum_value[1] * Get2piExp((2. * y2 + y3 + y4) * m + (y3 - y4) * k) +
////            exp_r_sum_value[2] * Get2piExp(-(y1 + y2 + y3 + y4) * k + 2. * y4 * m) +
////            exp_r_sum_value[3] * Get2piExp(2. * y4 * m + (y3 - y4) * k)
////    );
////    second_term = exp_p_sum_value * (
////            exp_r_sum_value[0] * exp_y_comb[0] * exp_y_comb[5] +
////            exp_r_sum_value[1] * exp_y_comb[5] * exp_y_comb[6] +
////            exp_r_sum_value[2] * exp_y_comb[0] * exp_y_comb[2] +
////            exp_r_sum_value[3] * exp_y_comb[2] * exp_y_comb[6]
////    );
//
//
//
//
//
////    for (int i = 0; i < 4; ++i)
////        std::cout << exp_r_sum_value[i];
////    std::cout << y1 << " " << y2 << " " << y3 << " " << y4 << " " << " " << first_term << " " << second_term << std::endl;
//
//    return result = first_term - 4. * second_term;
//}

inline std::complex<double> GetExSumBy2Var(double y4, double y3, double y2, double y1, A2SincParameters par) {
    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double k = (double)par.j_k; // index for calculation
    double m = (double)par.j_m;

    // Temp variables to accelerate calculations
    double y_sum[7];
    y_sum[0] = -(y1 + y2 + y3 + y4);
    y_sum[1] = y1 - y2 - y3 - y4;
    y_sum[2] = -y1 - y2 - y3 + y4;
    y_sum[3] = y1 - y2 - y3 + y4;

    y_sum[4] = 2. * (y2 + y3);
    y_sum[5] = 2. * y2 + y3 + y4;
    y_sum[6] = y3 - y4;

    std::complex<double> sum_exp_y_sum[5];
    for (int j = 0; j < 5; ++j) {
        sum_exp_y_sum[j] = GetSumExp(y_sum[j], par.M);
    }

    double y4_2_m = 2. * y4 * m;
    std::complex<double> exp_y_sum[7];
    exp_y_sum[0] = Get2piExp(y_sum[0] * k);
    exp_y_sum[1] = Get2piExp(y_sum[1] * k);
    exp_y_sum[2] = Get2piExp(y4_2_m);
    exp_y_sum[5] = Get2piExp(y_sum[5] * m);
    exp_y_sum[6] = Get2piExp(y_sum[6] * k);


    std::complex<double> first_term = 0.;
    std::complex<double> second_term = 0.;

//    std::complex<double> exp_p_sum_value = GetSumExp(2. * (y2 + y3), par.M);
    std::complex<double> exp_p_sum_value = sum_exp_y_sum[4];

    std::complex<double> exp_r_sum_value[4];
//    exp_r_sum_value[0] = GetSumExp(-y1 - y2 - y3 - y4, par.M);
//    exp_r_sum_value[1] = GetSumExp(y1 - y2 - y3 - y4, par.M);
//    exp_r_sum_value[2] = GetSumExp(-y1 - y2 - y3 + y4, par.M);
//    exp_r_sum_value[3] = GetSumExp(y1 - y2 - y3 + y4, par.M);
    for (int j = 0; j < 4; ++j) {
        exp_r_sum_value[j] = sum_exp_y_sum[j];
    }


//    first_term = exp_p_sum_value * (
//            exp_r_sum_value[0] * Get2piExp((y1 - y2 - y3 - y4) * k + 2. * y4 * m) +
//            exp_r_sum_value[1] * Get2piExp(-(y1 + y2 + y3 + y4) * k + 2. * y4 * m) +
//            exp_r_sum_value[2] * Get2piExp((y1 - y2 - y3 - y4) * k) +
//            exp_r_sum_value[3] * Get2piExp(-(y1 + y2 + y3 + y4) * k)
//    );
    first_term = exp_p_sum_value * (
            exp_r_sum_value[0] * exp_y_sum[1] * exp_y_sum[2] +
            exp_r_sum_value[1] * exp_y_sum[0] * exp_y_sum[2] +
            exp_r_sum_value[2] * exp_y_sum[1] +
            exp_r_sum_value[3] * exp_y_sum[0]
    );

//    exp_p_sum_value = GetSumExp(y1 - y2 - y3 - y4, par.M);
    exp_p_sum_value = sum_exp_y_sum[1];

    exp_r_sum_value[0] = GetSumExp(y3 + y4, par.M);
//    exp_r_sum_value[1] = GetSumExp(-y1 - y2 - y3 + y4, par.M);
    exp_r_sum_value[1] = sum_exp_y_sum[2];
//    exp_r_sum_value[2] = GetSumExp(2. * (y2 + y3), par.M);
    exp_r_sum_value[2] = sum_exp_y_sum[4];
    exp_r_sum_value[3] = GetSumExp(-y1 + y2, par.M);


//    second_term = exp_p_sum_value * (
//            exp_r_sum_value[0] * Get2piExp(-(y1 + y2 + y3 + y4) * k + (2. * y2 + y3 + y4) * m) +
//            exp_r_sum_value[1] * Get2piExp((2. * y2 + y3 + y4) * m + (y3 - y4) * k) +
//            exp_r_sum_value[2] * Get2piExp(-(y1 + y2 + y3 + y4) * k + 2. * y4 * m) +
//            exp_r_sum_value[3] * Get2piExp(2. * y4 * m + (y3 - y4) * k)
//    );
    second_term = exp_p_sum_value * (
            exp_r_sum_value[0] * exp_y_sum[0] * exp_y_sum[5] +
            exp_r_sum_value[1] * exp_y_sum[5] * exp_y_sum[6] +
            exp_r_sum_value[2] * exp_y_sum[0] * exp_y_sum[2] +
            exp_r_sum_value[3] * exp_y_sum[2] * exp_y_sum[6]
    );





//    for (int i = 0; i < 4; ++i)
//        std::cout << exp_r_sum_value[i];
//    std::cout << y1 << " " << y2 << " " << y3 << " " << y4 << " " << " " << first_term << " " << second_term << std::endl;

    return result = first_term - 4. * second_term;
}

inline std::complex<double> GetExKSumBy2Var(double y4, double y3, double y2, double y1, B2SincParameters par) {
    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double k = (double)par.j_k; // index for calculation
    double m = (double)par.j_m;

    std::complex<double> first_term = 0.;

    std::complex<double> exp_p_sum_value = GetSumExp(2. * (y2 + y3), par.M);
    std::complex<double> exp_r_sum_value[4];

    // Create temp variables to make calc faster
    double y_sum[4];
    y_sum[0] = -(y1 + y2 + y3 + y4);
    y_sum[1] = y1 - y2 - y3 - y4;
    y_sum[2] = -y1 - y2 - y3 + y4;
    y_sum[3] = y1 - y2 - y3 + y4;

    for (int j = 0; j < 4; ++j) {
        exp_r_sum_value[j] = GetSumExp(y_sum[j], par.M);
    }

    double y4_2_m = 2. * y4 * m;
    std::complex<double> exp_y_sum[3];
    exp_y_sum[0] = Get2piExp(y_sum[0] * k);
    exp_y_sum[1] = Get2piExp(y_sum[1] * k);
    exp_y_sum[2] = Get2piExp(y4_2_m);

    first_term = exp_p_sum_value * (
        exp_r_sum_value[0] * exp_y_sum[1] * exp_y_sum[2] +
        exp_r_sum_value[1] * exp_y_sum[0] * exp_y_sum[2] +
        exp_r_sum_value[2] * exp_y_sum[1] +
        exp_r_sum_value[3] * exp_y_sum[0]
    );
//    exp_r_sum_value[0] * Get2piExp((y1 - y2 - y3 - y4) * k + 2. * y4 * m) +
//    exp_r_sum_value[1] * Get2piExp(-(y1 + y2 + y3 + y4) * k + 2. * y4 * m) +
//    exp_r_sum_value[2] * Get2piExp((y1 - y2 - y3 - y4) * k) +
//    exp_r_sum_value[3] * Get2piExp(-(y1 + y2 + y3 + y4) * k)


//    for (int i = 0; i < 4; ++i)
//        std::cout << exp_r_sum_value[i];
//    std::cout << y1 << " " << y2 << " " << y3 << " " << y4 << " " << " " << first_term << " " << second_term << std::endl;

    // Don't forget then to multiply by (-2)
    return result = first_term;
}

inline std::complex<double> GetExASumOutTerm(double y4, double y3, double y2, double y1, A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    // For out [r, s1, s2; s3, s4, r]

    int s12_sum = par.m[1] + par.m[2];
    int s12_diff = par.m[1] - par.m[2];
    int s34_sum = par.m[3] + par.m[4];
    int s34_diff = par.m[3] - par.m[4];

    //

    double y_sum[4] = {2. * y4, 2. * y2 + y3 + y4, y3 - y4, -y1 - y2 - y3 - y4};

    result += 4. * GetSumExp(y1 - y2 - y3 - y4, par.M);

    if (s12_diff == 0 && s12_sum == 0) {
        // do nothing
    } else if (s12_diff == 0) {
        result *= exp(M_PI * im * (double)s12_sum * (y_sum[0] + y_sum[1]));
    } else if (s12_sum == 0) {
        result *= cos(M_PI * (double)s12_diff * (y_sum[0] - y_sum[1]));
    } else {
        result *= exp(M_PI * im * (double)s12_sum * (y_sum[0] + y_sum[1])) * cos(M_PI * (double)s12_diff * (y_sum[0] - y_sum[1]));
    }

    if (s34_diff == 0 && s34_sum == 0) {
        // do nothing
    } else if (s34_diff == 0) {
        result *= exp(M_PI * im * (double)s34_sum * (y_sum[2] + y_sum[3]));
    } else if (s34_sum == 0) {
        result *= cos(M_PI * (double)s34_diff * (y_sum[2] - y_sum[3]));
    } else {
        result *= exp(M_PI * im * (double)s34_sum * (y_sum[2] + y_sum[3])) * cos(M_PI * (double)s34_diff * (y_sum[2] - y_sum[3]));
    }

    return result;
}

inline std::complex<double> GetExASumInTerm(double y4, double y3, double y2, double y1, A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    // For out [s1, s2, r; r, s3, s4]

//    int s1 = par.m[0];
    int s2 = par.m[1];
    int s3 = par.m[4];
    int s4 = par.m[5];

    result += GetSumExp(2. * (y2 + y3), par.M);
    result *= Get2piExp(s2 * 2. * y4 + s3 * (-y1 - y2 - y3 - y4) + s4 * (y1 - y2 - y3 - y4));

    return result;
}


//std::complex<double> GetEx(double y4, double y3, double y2, double y1, A2SincParameters par);
inline std::complex<double> GetEx(double y4, double y3, double y2, double y1, A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;
    if (par.sum_key == 0) {
        result = exp(2. * M_PI * im * (-y1 * (double) (par.m[4] - par.m[5]) +
                                      y2 * (double) (2 * par.m[2] - par.m[4] - par.m[5]) +
                                      y3 * (double) (par.m[2] + par.m[3] - par.m[4] - par.m[5]) +
                                      y4 * (double) (2 * par.m[1] + par.m[2] - par.m[3] - par.m[4] - par.m[5])
        ));
    } else if (par.sum_key == 1 || par.sum_key == 2) {
        result = GetExSum(y4, y3, y2, y1, par);
    } else if (par.sum_key == 3) {
        result = GetExSumBy2Var(y4, y3, y2, y1, par);
//    } else if (par.sum_key == 4) {
//        result = TestGetExSumBy2Var(y4, y3, y2, y1, par);
    } else if (par.sum_key == 104) {
        result = GetExASumInTerm(y4, y3, y2, y1, par);
    } else if (par.sum_key == 105) {
        result = GetExASumOutTerm(y4, y3, y2, y1, par);
    } else {
        std::cout << "[GetEx]: you have not to be here" << std::endl;
    }

    return result;
}

inline std::complex<double> GetExK(double y4, double y3, double y2, double y1, B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    if (par.sum_key == 0) {
//        result = exp(2. * M_PI * im * (-y1 * (double) (par.k[2] - par.k[3]) +
//                                       -(y2 + y3) * (double) (par.k[2] + par.k[3]) +
//                                       y4 * (double) (2 * par.k[1] - par.k[2] - par.k[3])
//        ));

        result = Get2piExp(-y1 * (double)(par.k[2] - par.k[3]) -
                (y2 + y3) * (double)(par.k[2] + par.k[3]) +
                y4 * (double)(2 * par.k[1] - par.k[2] - par.k[3]));

//        std::complex<double> sum = 0.;
//        std::complex<double> exp_value = exp(4. * M_PI * im * (y2 + y3));
//        if (exp_value != 1.) {
//            sum = (exp(-4. * M_PI * im * (y2 + y3) * (double) par.M) -
//                   exp(4. * M_PI * im * (y2 + y3) * (double) (par.M + 1))) / (1. - exp_value);
//        } else {
//            for (int r = -par.M; r <= par.M; ++r) {
//                sum += exp(4. * M_PI * im * (y2 + y3) * (double) r);
//            }
//        }
//        result *= sum;

        result *= GetSumExp(2. * (y2 + y3), par.M);

    } else if (par.sum_key == 3) {
        result = GetExKSumBy2Var(y4, y3, y2, y1, par);
    } else {
        std::cout << "[GetExK]: you have not to be here" << std::endl;
    }

    return result;
}

inline std::complex<double> GetE5d(std::complex<double> a, std::complex<double> b) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    result = Sinc(a/2.) / 2.;
    std::complex<double> sum = 0.;
    std::complex<double> sum_next_element = 999.;
    std::complex<double> exp_pi_4 = exp(im * M_PI / 4.);

    double n = 1.;
    while (abs(sum_next_element) > 1e-12) {
        sum_next_element = (a * sin(a / 2.) * cosh(sqrt(b) / 2. * exp_pi_4 * n) + sqrt(b) * cos(a / 2.) * exp_pi_4 * n * sinh(sqrt(b) / 2. * exp_pi_4 * n) ) /
                (pow(a, 2) + im * b * n * n) * exp(-n*n / 4.);
        sum += sum_next_element;
        n += 1.;
        if (n > 100) {
            std::cout << "[GetE5d]: Error, iteration number > 100" << std::endl;
        }
    }
//    std::cout << n << std::endl;
    result = sqrt(M_1_PI) * (result + 2. * sum);

    return result;
}

std::vector<std::complex<double>>* GetG2Grid(A2SincParameters par);
std::vector<std::complex<double>>* GetG3Grid(B2SincParameters par);

std::complex<double> GetI(int n, A2SincParameters par);

std::complex<double> GetI1(A2SincParameters par);
std::complex<double> GetI1N0(A2SincParameters par);
std::complex<double> GetI1Nnot0(A2SincParameters par);

std::complex<double> GetI2(A2SincParameters par);
std::complex<double> GetI2N0(A2SincParameters par);
std::complex<double> GetI2Nnot0(A2SincParameters par);

std::complex<double> GetI3(A2SincParameters par);
std::complex<double> GetI3N0(A2SincParameters par);
std::complex<double> GetI3Nnot0(A2SincParameters par);

std::complex<double> GetI4(A2SincParameters par);
std::complex<double> GetI4N0(A2SincParameters par);
std::complex<double> GetI4Nnot0(A2SincParameters par);

std::complex<double> GetI5(A2SincParameters par);
std::complex<double> GetI5N0(A2SincParameters par);
std::complex<double> GetI5Nnot0(A2SincParameters par);

std::complex<double> GetI6(A2SincParameters par);
std::complex<double> GetI6N0(A2SincParameters par);
std::complex<double> GetI6Nnot0(A2SincParameters par);

std::complex<double> GetI7(A2SincParameters par);
std::complex<double> GetI7N0(A2SincParameters par);
std::complex<double> GetI7Nnot0(A2SincParameters par);

std::complex<double> GetI8(A2SincParameters par);
std::complex<double> GetI8N0(A2SincParameters par);
std::complex<double> GetI8Nnot0(A2SincParameters par);

std::complex<double> GetI9(A2SincParameters par);
std::complex<double> GetI9N0(A2SincParameters par);
std::complex<double> GetI9Nnot0(A2SincParameters par);

std::complex<double> GetI10(A2SincParameters par);
std::complex<double> GetI10N0(A2SincParameters par);
std::complex<double> GetI10Nnot0(A2SincParameters par);

std::complex<double> GetI11(A2SincParameters par);
std::complex<double> GetI11N0(A2SincParameters par);
std::complex<double> GetI11Nnot0(A2SincParameters par);

// Check with 5d integral
std::complex<double> GetA2with5d(A2SincParameters par);
std::complex<double> GetB2with5d(B2SincParameters par);

//std::complex<double> GetA2

// For B2

std::complex<double> GetI(int n, B2SincParameters par);

std::complex<double> GetI1(B2SincParameters par);
std::complex<double> GetI1N0(B2SincParameters par);
std::complex<double> GetI1Nnot0(B2SincParameters par);

std::complex<double> GetI2(B2SincParameters par);
std::complex<double> GetI2N0(B2SincParameters par);
std::complex<double> GetI2Nnot0(B2SincParameters par);

std::complex<double> GetI3(B2SincParameters par);
std::complex<double> GetI3N0(B2SincParameters par);
std::complex<double> GetI3Nnot0(B2SincParameters par);

std::complex<double> GetI4(B2SincParameters par);
std::complex<double> GetI4N0(B2SincParameters par);
std::complex<double> GetI4Nnot0(B2SincParameters par);

std::complex<double> GetI5(B2SincParameters par);
std::complex<double> GetI5N0(B2SincParameters par);
std::complex<double> GetI5Nnot0(B2SincParameters par);

std::complex<double> GetI6(B2SincParameters par);
std::complex<double> GetI6N0(B2SincParameters par);
std::complex<double> GetI6Nnot0(B2SincParameters par);

std::complex<double> GetI7(B2SincParameters par);
std::complex<double> GetI7N0(B2SincParameters par);
std::complex<double> GetI7Nnot0(B2SincParameters par);

std::complex<double> GetI9(B2SincParameters par);
std::complex<double> GetI9N0(B2SincParameters par);
std::complex<double> GetI9Nnot0(B2SincParameters par);

std::complex<double> GetI11(B2SincParameters par);
std::complex<double> GetI11N0(B2SincParameters par);
std::complex<double> GetI11Nnot0(B2SincParameters par);

// For A1 and B1

#ifndef AB1_PARAMETERS
#define AB1_PARAMETERS

struct Int1Parameters {
    double t_beta;
    int n;
    int m;
    int p;
    int k;
    int N;

    int integral_type; // 0 for a1 and 1 for b1
    int complex_type;

    double eps_abs = 1e-4;
    double eps_rel = 1e-4;
    int key = GSL_INTEG_GAUSS61;
    size_t n_intervals = 1000000;
};

struct IntT {
    Int1Parameters par;
    double y;
};

#endif // AB1_PARAMETERS

std::complex<double> Get1I(Int1Parameters par);

// General functions for calculations

template <typename T>
int CalculateIntegrals(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, T par) {

    auto time_point_one = std::chrono::steady_clock::now();
    auto time_point_two = std::chrono::steady_clock::now();
    auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);

    for (int k = 1; k <= 9; ++k) {
        time_point_one = std::chrono::steady_clock::now();
        if (k == 8) {
            integrals_values[k - 1] = GetI(9, par);
        } else if (k == 9) {
            integrals_values[k - 1] = GetI(11, par);
        } else {
            integrals_values[k - 1] = GetI(k, par);
        }
        time_point_two = std::chrono::steady_clock::now();
        diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point_two - time_point_one);
        integrals_ms[k - 1] = (int) diff_ms.count();
    }

    return 0;
}

// A2: [s1, s2, r; r, s3, s4] or [r, s1, s2; s3, s4, r] + ...
int CalculateSumTermA2(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* s, A2SincParameters par);

// J[r,m;r,k] + J[r,m;k,r] + J[m,r;r,k] + J[m,r;k,r]
// (Only A2 part)
int CalculateJSumA2(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, A2SincParameters par);
// J_Lambda[r,m;r,k] + J_Lambda[r,m;k,r] + J_Lambda[m,r;r,k] + J_Lambda[m,r;k,r]
// (Only B2 part)
int CalculateJLambdaSumB2(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, B2SincParameters par);

int PrintIntegrals(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms);

int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double eps_rel, double eps_abs);
int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, double eps_rel, double eps_abs);
int WriteIntegralsToFile(std::fstream& file, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double eps_rel, double eps_abs);

int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double* eps_rel, double* eps_abs, int* key);
int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, double* eps_rel, double* eps_abs, int* key);
int WriteIntegralsToFile(std::fstream& file, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double* eps_rel, double* eps_abs, int* key);

// max_value -- maximum M
// var_value -- M*2 + 1
inline int* CalculateIndex(int var, int max_value, int var_value, int n_index) {

    int* index = new int[n_index];
    if (n_index == 6) {
        index[0] = var % var_value - max_value;
        index[1] = (var / var_value) % var_value - max_value;
        index[2] = ((var / var_value) / var_value) % var_value - max_value;
        index[3] = (((var / var_value) / var_value) / var_value) % var_value - max_value;
        index[4] = ((((var / var_value) / var_value) / var_value) / var_value) % var_value - max_value;
        index[5] =
                (((((var / var_value) / var_value) / var_value) / var_value) / var_value) % var_value -
                max_value;
    } else if (n_index == 4) {
        index[0] = var % var_value - max_value;
        index[1] = (var / var_value) % var_value - max_value;
        index[2] = ((var / var_value) / var_value) % var_value - max_value;
        index[3] = (((var / var_value) / var_value) / var_value) % var_value - max_value;
    } else if (n_index == 2) {
        index[0] = var % var_value - max_value;
        index[1] = (var / var_value) % var_value - max_value;
    } else if (n_index == 3) {
        index[0] = var % var_value - max_value;
        index[1] = (var / var_value) % var_value - max_value;
        index[2] = ((var / var_value) / var_value) % var_value - max_value;
    } else {
        std::cout << "[CalculateIndex]: Undefined situation" << std::endl;
    }

    return index;
}

int* CalculateHalfIndex(int index_var, int m_max, int n_index);

//void PrintA2Parameters(A2SincParameters par) {
//    std::cout << "t_beta = " << t_beta << std::endl;
//    std::cout << "m1 m2 m3 m4 m5 m6" << std::endl
//    for (int k = 0; k < 6; ++k)
//        std::cout << par.m[k];
//    std::cout << std::endl;
//    std::cout << "N = " << par.N << std::endl;
//    std::cout << "j_k = " << par.j_k << std::endl;
//    std::cout << "j_m = " << par.j_m << std::endl;
//
//    std::cout << "M = " << par.M << std::endl;
//    int M = 5;
//    int sum_key = 0; // 0 - without sum, 1 - sum type 1 (m2 and m4), 2 - sum type 2 (m2 and m5), 3 - J (A2 term) calc
//
//    double eps_abs = 1e-4;
//    double eps_rel = 1e-4;
//    int key = GSL_INTEG_GAUSS61;
//    size_t n_intervals = 10000000;
//
//    double eps_abs_ratio[4] = {1e-4, 1e-4, 1e-4, 1e-4};
//    double eps_rel_ratio[4] = {1e-4, 1e-4, 1e-4, 1e-4};
//    int key_ratio[4] = {GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61, GSL_INTEG_GAUSS61};
//
//    double a_start = -1.0;
//    double a_end = 1.0;
//    double b_start = -1.0;
//    double b_end = 1.0;
//    int n_integral = pow(2, 13);
//    std::vector<std::complex<double>>* g2_m = NULL;
//}


#endif //OPTIMALCHANNELINT_INTEGRALS_H
