//
// Created by esf0 on 12.02.2020.
//

#include "integrals.h"

using namespace std::complex_literals; // TODO: it is for cluster

// Implementation

void swap(double* a, double* b, double* sign) {
    double temp = *a;
    *a = *b;
    *b = temp;
    *sign = -1.0;
}

std::complex<double> Sinc(std::complex<double> x) {
    if (x == 0.)
        return 1.;
    else
        return sin(x) / x;
}


//int GetN(A2SincParameters par) {
//    int N = par.m[0] + par.m[1] + par.m[2] - par.m[3] - par.m[4] - par.m[5];
//    return N;
//}

//std::complex<double> GetG2(double a, double b, A2SincParameters par) {
//
////    if (par.g2_m != NULL) {
////        double a_pos = (a - par.a_start) / (par.a_end - par.a_start) * (double)par.n_integral;
////        double b_pos = (b - par.b_start) / (par.b_end - par.b_start) * (double)par.n_integral;
////        int a_floor = floor(a_pos), a_ceil = ceil(a_pos);
////        int b_floor = floor(b_pos), b_ceil = ceil(b_pos);
////
////        double a_part = a_pos - (double)a_floor;
////        double b_part = b_pos - (double)b_floor;
////
////        std::complex<double> result = 0.;
////        result = ( par.g2_m->at(a_floor * par.n_integral + b_floor) * (1. - a_part) + par.g2_m->at(a_ceil * par.n_integral + b_floor) * a_part ) * (1. - b_part) +
////                 ( par.g2_m->at(a_floor * par.n_integral + b_ceil) * (1. - a_part) + par.g2_m->at(a_ceil * par.n_integral + b_ceil) * a_part ) * b_part;
////        return result;
////    }
//
//    std::complex<double> im = 1i;
//    std::complex<double> k = 4. * im * par.t_beta;
//    std::complex<double> result = 0.;
//    double tolerance = 1e-4;
//
//    if (abs(a) < tolerance) {
//        if (abs(b) < tolerance) {
//            result = (0.5 + k * a / 3. + pow(k * a, 2) / 8. + pow(k * a, 3) / 30.) + (k / 6. + pow(k, 2) * a / 8. + pow(k * a, 2) * k / 20.) * b;
//        } else {
//            result = -1. / (16. * pow(par.t_beta, 2) * b) *
//                     ((exp(4. * im * par.t_beta * (a + b)) - 1.) / (a + b) - (k - pow(k, 2) * a * 0.5 - pow(k, 3) * pow(a, 2) / 6.));
//        }
//    } else if (abs(b) < tolerance) {
//        std::complex<double> exp_ka = exp(k * a);
//        std::complex<double> k_m2 = pow(k, -2);
//        result = k_m2 * pow(a, -2) * (1. + (k * a - 1.) * exp_ka ) + b * k_m2 * pow(a, -3) * 0.5 * (-2. + exp_ka * (2. - 2. * k * a + pow(k * a, 2))) +
//                pow(b, 2) * k_m2 * pow(a, -4) / 6. * (6. + exp_ka * (-6. + 6. * k * a - 3. * pow(k * a, 2) + pow(k * a, 3))) +
//                pow(b, 3) * k_m2 * pow(a, -5) / 24. * (-24. + exp_ka * (24. - 24. * k * a + 12. * pow(k * a, 2) - 4. * pow(k * a, 3) + pow(k * a, 4)));
////        result = k_m2 * pow(a, -2) * (1. + k * a * exp_ka - exp_ka);
//    } else {
//        result = -1. / (16. * pow(par.t_beta, 2) * b) *
//                 ((exp(k * (a + b)) - 1.) / (a + b) - (exp(k * a) - 1.) / a);
//    }
//
//    return result;
//}

//std::complex<double> GetEx(double y4, double y3, double y2, double y1, A2SincParameters par) {
//
//    std::complex<double> im = 1i;
//    std::complex<double> result = 0.;
//    result = exp(2 * M_PI * im * (-y1 * (par.m[4] - par.m[5]) +
//            y2 * (2*par.m[2] - par.m[4] - par.m[5]) +
//            y3 * (par.m[2] + par.m[3] - par.m[4] - par.m[5]) +
//            y4 * (2*par.m[1] + par.m[2] - par.m[3] - par.m[4] - par.m[5])));
//
//    return result;
//}

std::vector<std::complex<double>>* GetG2Grid(A2SincParameters par) {

    double da = (par.a_end - par.a_start) / (double)par.n_integral;
    double db = (par.b_end - par.b_start) / (double)par.n_integral;
    auto matrix = new std::vector<std::complex<double>>((par.n_integral + 1) * (par.n_integral + 1));

    for (int ai = 0; ai <= par.n_integral; ++ai) {
        for (int bi = 0; bi <= par.n_integral; ++bi) {
            (*matrix)[ai * (par.n_integral + 1) + bi] = GetG2(ai * da + par.a_start, bi * db + par.b_start, par);
        }
    }

    return matrix;
}

std::vector<std::complex<double>>* GetG3Grid(B2SincParameters par) {

    double da = (par.a_end - par.a_start) / (double)par.n_integral;
    double db = (par.b_end - par.b_start) / (double)par.n_integral;
    auto matrix = new std::vector<std::complex<double>>((par.n_integral + 1) * (par.n_integral + 1));

    for (int ai = 0; ai <= par.n_integral; ++ai) {
        for (int bi = 0; bi <= par.n_integral; ++bi) {
            (*matrix)[ai * (par.n_integral + 1) + bi] = GetG3(ai * da + par.a_start, bi * db + par.b_start, par);
        }
    }

    return matrix;
}

std::complex<double> GetI(int n, A2SincParameters par) {
    switch (n) {
        case 1: {
            return GetI1(par);
            break;
        }
        case 2: {
            return GetI2(par);
            break;
        }
        case 3: {
            return GetI3(par);
            break;
        }
        case 4: {
            return GetI4(par);
            break;
        }
        case 5: {
            return GetI5(par);
            break;
        }
        case 6: {
            return GetI6(par);
            break;
        }
        case 7: {
            return GetI7(par);
            break;
        }
        case 9: {
            return GetI9(par);
            break;
        }
        case 11: {
            return GetI11(par);
            break;
        }
        default: {
            return 0.;
            break;
        }
    }
}

std::complex<double> GetI1(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI1N0(par);
    } else {
        return GetI1Nnot0(par);
    }
}

std::complex<double> GetI1N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt1Y4(par, 0);
    double imag_part = GetInt1Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI1Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt1Y4(par, 0);
    double imag_part = GetInt1Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2. * exp(im * M_PI * (double)GetN(par)) / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI2(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI2N0(par);
    } else {
        return GetI2Nnot0(par);
    }
}

std::complex<double> GetI2N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt2Y4(par, 0);
    double imag_part = GetInt2Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI2Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt2Y4(par, 0);
    double imag_part = GetInt2Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2. * exp(-1. * im * M_PI * (double)GetN(par)) / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI3(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI3N0(par);
    } else {
        return GetI3Nnot0(par);
    }
}

std::complex<double> GetI3N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt3Y5N0(par, 0);
    double imag_part = GetInt3Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI3Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt3Y5Nnot0(par, 0);
    double imag_part = GetInt3Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI4(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI4N0(par);
    } else {
        return GetI4Nnot0(par);
    }
}

std::complex<double> GetI4N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt4Y5N0(par, 0);
    double imag_part = GetInt4Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2.;

    return result;
}

std::complex<double> GetI4Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt4Y5Nnot0(par, 0);
    double imag_part = GetInt4Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI5(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI5N0(par);
    } else {
        return GetI5Nnot0(par);
    }
}

std::complex<double> GetI5N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt5Y5N0(par, 0);
    double imag_part = GetInt5Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -4.;

    return result;
}

std::complex<double> GetI5Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt5Y5Nnot0(par, 0);
    double imag_part = GetInt5Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI6(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI6N0(par);
    } else {
        return GetI6Nnot0(par);
    }
}

std::complex<double> GetI6N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt6Y5N0(par, 0);
    double imag_part = GetInt6Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 4.;

    return result;
}

std::complex<double> GetI6Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt6Y5Nnot0(par, 0);
    double imag_part = GetInt6Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI7(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI7N0(par);
    } else {
        return GetI7Nnot0(par);
    }
}

std::complex<double> GetI7N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt7Y5N0(par, 0);
    double imag_part = GetInt7Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI7Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt7Y5Nnot0(par, 0);
    double imag_part = GetInt7Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI8(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI8N0(par);
    } else {
        return GetI8Nnot0(par);
    }
}

std::complex<double> GetI8N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;
    return std::complex<double>();
}

std::complex<double> GetI8Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;
    return std::complex<double>();
}


std::complex<double> GetI9(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI9N0(par);
    } else {
        return GetI9Nnot0(par);
    }
}

std::complex<double> GetI9N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt9Y5N0(par, 0);
    double imag_part = GetInt9Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2.;

    return result;
}

std::complex<double> GetI9Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt9Y5Nnot0(par, 0);
    double imag_part = GetInt9Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI10(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI10N0(par);
    } else {
        return GetI10Nnot0(par);
    }
}

std::complex<double> GetI10N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;
    return std::complex<double>();
}

std::complex<double> GetI10Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;
    return std::complex<double>();
}


std::complex<double> GetI11(A2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI11N0(par);
    } else {
        return GetI11Nnot0(par);
    }
}

std::complex<double> GetI11N0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt11Y5N0(par, 0);
    double imag_part = GetInt11Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 4.;

    return result;
}

std::complex<double> GetI11Nnot0(A2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetInt11Y5Nnot0(par, 0);
    double imag_part = GetInt11Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2. / (im * M_PI * (double)GetN(par));

    return result;
}



std::complex<double> GetA2with5d(A2SincParameters par) {
    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetIntC5d_t1(par, 0);
    double imag_part = GetIntC5d_t1(par, 1);

    result.real(real_part);
    result.imag(imag_part);

    return result;
}

std::complex<double> GetB2with5d(B2SincParameters par) {

    std::complex<double> result = 0.;

    double real_part = GetIntCB5d_z1(par, 0);
    double imag_part = GetIntCB5d_z1(par, 1);

    result.real(real_part);
    result.imag(imag_part);

    return result;
}


// For B2

std::complex<double> GetI(int n, B2SincParameters par) {
    switch (n) {
        case 1: {
            return GetI1(par);
            break;
        }
        case 2: {
            return GetI2(par);
            break;
        }
        case 3: {
            return GetI3(par);
            break;
        }
        case 4: {
            return GetI4(par);
            break;
        }
        case 5: {
            return GetI5(par);
            break;
        }
        case 6: {
            return GetI6(par);
            break;
        }
        case 7: {
            return GetI7(par);
            break;
        }
        case 9: {
            return GetI9(par);
            break;
        }
        case 11: {
            return GetI11(par);
            break;
        }
        default: {
            return 0.;
            break;
        }
    }
}

std::complex<double> GetI1(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI1N0(par);
    } else {
        return GetI1Nnot0(par);
    }
}

std::complex<double> GetI1N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt1Y4(par, 0);
    double imag_part = GetBInt1Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI1Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt1Y4(par, 0);
    double imag_part = GetBInt1Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2. * exp(im * M_PI * (double)GetN(par)) / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI2(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI2N0(par);
    } else {
        return GetI2Nnot0(par);
    }
}

std::complex<double> GetI2N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt2Y4(par, 0);
    double imag_part = GetBInt2Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI2Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt2Y4(par, 0);
    double imag_part = GetBInt2Y4(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2. * exp(-1. * im * M_PI * (double)GetN(par)) / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI3(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI3N0(par);
    } else {
        return GetI3Nnot0(par);
    }
}

std::complex<double> GetI3N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt3Y5N0(par, 0);
    double imag_part = GetBInt3Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI3Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt3Y5Nnot0(par, 0);
    double imag_part = GetBInt3Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI4(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI4N0(par);
    } else {
        return GetI4Nnot0(par);
    }
}

std::complex<double> GetI4N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt4Y5N0(par, 0);
    double imag_part = GetBInt4Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2.;

    return result;
}

std::complex<double> GetI4Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt4Y5Nnot0(par, 0);
    double imag_part = GetBInt4Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI5(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI5N0(par);
    } else {
        return GetI5Nnot0(par);
    }
}

std::complex<double> GetI5N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt5Y5N0(par, 0);
    double imag_part = GetBInt5Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -4.;

    return result;
}

std::complex<double> GetI5Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt5Y5Nnot0(par, 0);
    double imag_part = GetBInt5Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI6(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI6N0(par);
    } else {
        return GetI6Nnot0(par);
    }
}

std::complex<double> GetI6N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt6Y5N0(par, 0);
    double imag_part = GetBInt6Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 4.;

    return result;
}

std::complex<double> GetI6Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt6Y5Nnot0(par, 0);
    double imag_part = GetBInt6Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI7(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI7N0(par);
    } else {
        return GetI7Nnot0(par);
    }
}

std::complex<double> GetI7N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt7Y5N0(par, 0);
    double imag_part = GetBInt7Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2.;

    return result;
}

std::complex<double> GetI7Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt7Y5Nnot0(par, 0);
    double imag_part = GetBInt7Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI9(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI9N0(par);
    } else {
        return GetI9Nnot0(par);
    }
}

std::complex<double> GetI9N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt9Y5N0(par, 0);
    double imag_part = GetBInt9Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -2.;

    return result;
}

std::complex<double> GetI9Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt9Y5Nnot0(par, 0);
    double imag_part = GetBInt9Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= -1. / (im * M_PI * (double)GetN(par));

    return result;
}


std::complex<double> GetI11(B2SincParameters par) {

    if (GetN(par) == 0) {
        return GetI11N0(par);
    } else {
        return GetI11Nnot0(par);
    }
}

std::complex<double> GetI11N0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt11Y5N0(par, 0);
    double imag_part = GetBInt11Y5N0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 4.;

    return result;
}

std::complex<double> GetI11Nnot0(B2SincParameters par) {

    std::complex<double> im = 1i;
    std::complex<double> result = 0.;

    double real_part = GetBInt11Y5Nnot0(par, 0);
    double imag_part = GetBInt11Y5Nnot0(par, 1);

    result.real(real_part);
    result.imag(imag_part);
    result *= 2. / (im * M_PI * (double)GetN(par));

    return result;
}

// General functions for calculations

// A2: [s1, s2, r; r, s3, s4] or [r, s1, s2; s3, s4, r] + ...
int CalculateSumTermA2(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* s, A2SincParameters par) {

    if (par.sum_key == 104) {
        par.m[0] = s[0];
        par.m[1] = s[1];
        par.m[2] = 0;
        par.m[3] = 0;
        par.m[4] = s[2];
        par.m[5] = s[3];
    } else {
        par.m[0] = 0;
        par.m[1] = s[0];
        par.m[2] = s[1];
        par.m[3] = s[2];
        par.m[4] = s[3];
        par.m[5] = 0;
    }
    par.N = GetN(par);

    CalculateIntegrals(integrals_values, integrals_ms, par);

    return 0;
}

// J[r,m;r,k] + J[r,m;k,r] + J[m,r;r,k] + J[m,r;k,r] (Only A2 part)
int OldCalculateJSumA2(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, A2SincParameters par) {

    std::vector<std::complex<double>> direct_integrals_values(9);
    std::vector<int> direct_integrals_ms(9);
    std::vector<std::complex<double>> inverse_integrals_values(9);
    std::vector<int> inverse_integrals_ms(9);

    par.sum_key = 3;

    par.m[0] = 0;
    par.m[1] = 0;
    par.m[2] = m;
    par.m[3] = k;
    par.m[4] = 0;
    par.m[5] = 0;
    par.N = GetN(par);
//    std::cout << par.N << std::endl;
    par.j_m = m;
    par.j_k = k;

    CalculateIntegrals(direct_integrals_values, direct_integrals_ms, par);

    if (k != m) {
        // Change k and m
        par.m[2] = k;
        par.m[3] = m;
        par.N = GetN(par);
//        std::cout << par.N << std::endl;
        par.j_m = -k; // "-" sign due to complex conj in A2
        par.j_k = -m;

        CalculateIntegrals(inverse_integrals_values, inverse_integrals_ms, par);
    }

    for (int i = 0; i < 9; i++) {
        if (k == m) {
            integrals_values[i] = 2. * direct_integrals_values[i].real(); // direct_integrals_values[i] + conj(direct_integrals_values[i])
            integrals_ms[i] = direct_integrals_ms[i];
        } else {
            integrals_values[i] = direct_integrals_values[i] + inverse_integrals_values[i];
            integrals_ms[i] = direct_integrals_ms[i] + inverse_integrals_ms[i];
        }
    }

    return 0;
}

// J[r,m;r,k] + J[r,m;k,r] + J[m,r;r,k] + J[m,r;k,r] (Only A2 part)
int CalculateJSumA2(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, A2SincParameters par) {

    par.sum_key = 3; // TODO: check and change general function

    par.m[0] = 0;
    par.m[1] = 0;
    par.m[2] = m;
    par.m[3] = k;
    par.m[4] = 0;
    par.m[5] = 0;
    par.N = GetN(par);
//    std::cout << par.N << std::endl;
    par.j_m = m;
    par.j_k = k;

    CalculateIntegrals(integrals_values, integrals_ms, par);

    return 0;
}

int CalculateJLambdaSumB2(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, B2SincParameters par) {

    par.sum_key = 3;

    par.k[0] = 0;
    par.k[1] = m;
    par.k[2] = k;
    par.k[3] = 0;
    par.N = GetN(par);
    par.j_m = m;
    par.j_k = k;

    CalculateIntegrals(integrals_values, integrals_ms, par);
    for (int i = 0; i < 9; ++i) {
        integrals_values[i] *= -2.;
    }

    return 0;

}

int PrintIntegrals(std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms) {
    double real_total = 0., imag_total = 0.;
    for (int k = 0; k < 9; ++k) {
        std::cout << integrals_values[k].real() << " " << integrals_values[k].imag() << " " << integrals_ms[k]
                  << std::endl;
        real_total += integrals_values[k].real();
        imag_total += integrals_values[k].imag();
    }
    std::cout << real_total << " " << imag_total << std::endl;

    return 0;
}

int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double eps_rel, double eps_abs) {

    std::fstream file;
    file.open(file_name, std::ios::out | std::ios::app);

    WriteIntegralsToFile(file, integrals_values, integrals_ms, coef, n_coef, eps_rel, eps_abs);

    file.close();

    return 0;
}

int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, double eps_rel, double eps_abs) {
    int coef[2];
    coef[0] = m;
    coef[1] = k;
    return WriteIntegralsToFile(file_name, integrals_values, integrals_ms, coef, 2, eps_rel, eps_abs);
}


int WriteIntegralsToFile(std::fstream& file, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double eps_rel, double eps_abs) {
    for (int k = 0; k < n_coef; ++k)
        file << coef[k] << " ";
    file << eps_rel << " " << eps_abs << std::endl;

    file << std::setprecision(20);

    double real_total = 0., imag_total = 0.;
    for (int k = 0; k < 9; ++k) {
        file << integrals_values[k].real() << " " << integrals_values[k].imag() << " " << integrals_ms[k]
             << std::endl;
        real_total += integrals_values[k].real();
        imag_total += integrals_values[k].imag();
    }
    file << real_total << " " << imag_total << std::endl;

    return 0;
}

int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double* eps_rel, double* eps_abs, int* key) {
    std::fstream file;
    file.open(file_name, std::ios::out | std::ios::app);

    WriteIntegralsToFile(file, integrals_values, integrals_ms, coef, n_coef, eps_rel, eps_abs, key);

    file.close();

    return 0;
}

int WriteIntegralsToFile(std::string& file_name, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int m, int k, double* eps_rel, double* eps_abs, int* key) {
    int coef[2];
    coef[0] = m;
    coef[1] = k;
    return WriteIntegralsToFile(file_name, integrals_values, integrals_ms, coef, 2, eps_rel, eps_abs, key);
}


int WriteIntegralsToFile(std::fstream& file, std::vector<std::complex<double>>& integrals_values, std::vector<int>& integrals_ms, int* coef, int n_coef, double* eps_rel, double* eps_abs, int* key) {
    for (int k = 0; k < n_coef; ++k)
        file << coef[k] << " ";
    for (int k = 0; k < 4; ++k) {
        file << eps_rel[k] << " ";
    }
    for (int k = 0; k < 4; ++k) {
        file << eps_abs[k] << " ";
    }
    for (int k = 0; k < 4; ++k) {
        file << key[k] << " ";
    }
    file << std::endl;

    file << std::setprecision(20);

    double real_total = 0., imag_total = 0.;
    for (int k = 0; k < 9; ++k) {
        file << integrals_values[k].real() << " " << integrals_values[k].imag() << " " << integrals_ms[k]
             << std::endl;
        real_total += integrals_values[k].real();
        imag_total += integrals_values[k].imag();
    }
    file << real_total << " " << imag_total << std::endl;

    return 0;
}

int* CalculateHalfIndex(int index_var, int m_max, int n_index) {

    if (n_index != 4) {
        std::cout << "Wrong n_index number" << std::endl;
        return NULL;
    }

    int* s = new int[n_index];

    bool _break = false;
    int index_cur = 0;
    for (int s1 = -m_max; s1 <= m_max; ++s1) {
        for (int s2 = -m_max; s2 <= s1; ++s2) {
            for (int s3 = -m_max; s3 <= m_max; ++s3) {
                for (int s4 = -m_max; s4 <= s3; ++s4) {
                    if (index_cur == index_var) {
                        s[0] = s1;
                        s[1] = s2;
                        s[2] = s3;
                        s[3] = s4;
//                        std::cout << s1 << " " << s2 << " " << s3 << " " << s4 << " " << std::endl;
                        _break = true;
                        break;
                    } else {
                        index_cur++;
                    }
                }
                if (_break) {
                    break;
                }
            }
            if (_break) {
                break;
            }
        }
        if (_break) {
            break;
        }
    }

    return s;
}