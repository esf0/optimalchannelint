//
// Created by esf0 on 06.04.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_CHECK_INTEGRAL_5D_H
#define OPTIMALCHANNELINT_A2SINC_CHECK_INTEGRAL_5D_H

#include "integrals.h"

struct IntC5d_t1 {
    A2SincParameters a2p;
    int type;
};

struct IntC5d_t2 {
    A2SincParameters a2p;
    int type;
    double t1;
};

struct IntC5d_z1 {
    A2SincParameters a2p;
    int type;
    double t1;
    double t2;
};

struct IntC5d_z2 {
    A2SincParameters a2p;
    int type;
    double t1;
    double t2;
    double z1;
};

double GetIntC5dCore(double y2, void* par);
double GetIntC5d_z2(double y3, void* par);
double GetIntC5d_z1(double y4, void* par);
double GetIntC5d_t2(double y5, void* par);
double GetIntC5d_t1(A2SincParameters a2p, int type);

#endif //OPTIMALCHANNELINT_A2SINC_CHECK_INTEGRAL_5D_H
