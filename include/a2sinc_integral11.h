//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL11_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL11_H

#include "integrals.h"

struct Int11y5 {
    A2SincParameters a2p;
    int type;
};

struct Int11y4 {
    A2SincParameters a2p;
    int type;
    double y5;
};

struct Int11y3 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
};

struct Int11y2 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
    double y3;
};

double GetInt11Core(double y2, void* par);
double GetInt11Y2(double y3, void* par);
double GetInt11Y3(double y4, void* par);
double GetInt11Y4N0(double y5, void* par);
double GetInt11Y4Nnot0(double y5, void* par);
double GetInt11Y5N0(A2SincParameters a2p, int type);
double GetInt11Y5Nnot0(A2SincParameters a2p, int type);

#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL11_H
