//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL6_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL6_H

#include "integrals.h"

struct BInt6y5 {
    B2SincParameters b2p;
    int type;
};

struct BInt6y4 {
    B2SincParameters b2p;
    int type;
    double y5;
};

struct BInt6y2 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
};

struct BInt6y1 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
    double y2;
};

double GetBInt6Core(double y1, void* par);
double GetBInt6Y1(double y2, void* par);
double GetBInt6Y2(double y4, void* par);
double GetBInt6Y4N0(double y5, void* par);
double GetBInt6Y4Nnot0(double y5, void* par);
double GetBInt6Y5N0(B2SincParameters b2p, int type);
double GetBInt6Y5Nnot0(B2SincParameters b2p, int type);

#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL6_H
