//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL4_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL4_H

#include "integrals.h"

struct Int4y5 {
    A2SincParameters a2p;
    int type;
};

struct Int4y3 {
    A2SincParameters a2p;
    int type;
    double y5;
};

struct Int4y2 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y3;
};

struct Int4y1 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y3;
    double y2;
};

double GetInt4Core(double y1, void* par);
double GetInt4Y1(double y2, void* par);
double GetInt4Y2(double y3, void* par);
double GetInt4Y3N0(double y5, void* par);
double GetInt4Y3Nnot0(double y5, void* par);
double GetInt4Y5N0(A2SincParameters a2p, int type);
double GetInt4Y5Nnot0(A2SincParameters a2p, int type);

#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL4_H
