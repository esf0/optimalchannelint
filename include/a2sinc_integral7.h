//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL7_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL7_H

#include "integrals.h"

struct Int7y5 {
    A2SincParameters a2p;
    int type;
};

struct Int7y4 {
    A2SincParameters a2p;
    int type;
    double y5;
};

struct Int7y3 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
};

struct Int7y1 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
    double y3;
};

double GetInt7Core(double y1, void* par);
double GetInt7Y1(double y3, void* par);
double GetInt7Y3(double y4, void* par);
double GetInt7Y4N0(double y5, void* par);
double GetInt7Y4Nnot0(double y5, void* par);
double GetInt7Y5N0(A2SincParameters a2p, int type);
double GetInt7Y5Nnot0(A2SincParameters a2p, int type);

#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL7_H
