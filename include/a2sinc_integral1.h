//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL1_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL1_H

#include "integrals.h"

struct Int1y4 {
    A2SincParameters a2p;
    int type;
};

struct Int1y3 {
    A2SincParameters a2p;
    int type;
    double y4;
};

struct Int1y2 {
    A2SincParameters a2p;
    int type;
    double y4;
    double y3;
};

struct Int1y1 {
    A2SincParameters a2p;
    int type;
    double y4;
    double y3;
    double y2;
};

double GetInt1Core(double y1, void* par);
double GetInt1Y1(double y2, void* par);
double GetInt1Y2(double y3, void* par);
double GetInt1Y3(double y4, void* par);
double GetInt1Y4(A2SincParameters a2p, int type);

#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL1_H
