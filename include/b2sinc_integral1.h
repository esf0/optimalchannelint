//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL1_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL1_H

#include "integrals.h"

struct BInt1y4 {
    B2SincParameters b2p;
    int type;
};

struct BInt1y3 {
    B2SincParameters b2p;
    int type;
    double y4;
};

struct BInt1y2 {
    B2SincParameters b2p;
    int type;
    double y4;
    double y3;
};

struct BInt1y1 {
    B2SincParameters b2p;
    int type;
    double y4;
    double y3;
    double y2;
};

double GetBInt1Core(double y1, void* par);
double GetBInt1Y1(double y2, void* par);
double GetBInt1Y2(double y3, void* par);
double GetBInt1Y3(double y4, void* par);
double GetBInt1Y4(B2SincParameters b2p, int type);

#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL1_H
