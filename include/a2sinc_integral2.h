//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL2_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL2_H

#include "integrals.h"

struct Int2y4 {
    A2SincParameters a2p;
    int type;
};

struct Int2y3 {
    A2SincParameters a2p;
    int type;
    double y4;
};

struct Int2y2 {
    A2SincParameters a2p;
    int type;
    double y4;
    double y3;
};

struct Int2y1 {
    A2SincParameters a2p;
    int type;
    double y4;
    double y3;
    double y2;
};

double GetInt2Core(double y1, void* par);
double GetInt2Y1(double y2, void* par);
double GetInt2Y2(double y3, void* par);
double GetInt2Y3(double y4, void* par);
double GetInt2Y4(A2SincParameters a2p, int type);


#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL2_H
