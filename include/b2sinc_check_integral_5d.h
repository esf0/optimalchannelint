//
// Created by esf0 on 06.04.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_CHECK_INTEGRAL_5D_H
#define OPTIMALCHANNELINT_B2SINC_CHECK_INTEGRAL_5D_H

#include "integrals.h"

struct IntCB5d_z1 {
    B2SincParameters b2p;
    int type;
};

struct IntCB5d_z2 {
    B2SincParameters b2p;
    int type;
    double z1;
};

struct IntCB5d_a {
    B2SincParameters b2p;
    int type;
    double z1;
    double z2;
};

struct IntCB5d_z {
    B2SincParameters b2p;
    int type;
    double z1;
    double z2;
    double a;
};

double GetIntCB5dCore(double z, void* par);
double GetIntCB5d_z(double a, void* par);
double GetIntCB5d_a(double z2, void* par);
double GetIntCB5d_z2(double z1, void* par);
double GetIntCB5d_z1(B2SincParameters b2p, int type);

#endif //OPTIMALCHANNELINT_B2SINC_CHECK_INTEGRAL_5D_H
