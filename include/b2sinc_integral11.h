//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL11_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL11_H

#include "integrals.h"

struct BInt11y5 {
    B2SincParameters b2p;
    int type;
};

struct BInt11y4 {
    B2SincParameters b2p;
    int type;
    double y5;
};

struct BInt11y3 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
};

struct BInt11y2 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
    double y3;
};

double GetBInt11Core(double y2, void* par);
double GetBInt11Y2(double y3, void* par);
double GetBInt11Y3(double y4, void* par);
double GetBInt11Y4N0(double y5, void* par);
double GetBInt11Y4Nnot0(double y5, void* par);
double GetBInt11Y5N0(B2SincParameters b2p, int type);
double GetBInt11Y5Nnot0(B2SincParameters b2p, int type);

#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL11_H
