//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL6_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL6_H

#include "integrals.h"

struct Int6y5 {
    A2SincParameters a2p;
    int type;
};

struct Int6y4 {
    A2SincParameters a2p;
    int type;
    double y5;
};

struct Int6y2 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
};

struct Int6y1 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
    double y2;
};

double GetInt6Core(double y1, void* par);
double GetInt6Y1(double y2, void* par);
double GetInt6Y2(double y4, void* par);
double GetInt6Y4N0(double y5, void* par);
double GetInt6Y4Nnot0(double y5, void* par);
double GetInt6Y5N0(A2SincParameters a2p, int type);
double GetInt6Y5Nnot0(A2SincParameters a2p, int type);

#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL6_H
