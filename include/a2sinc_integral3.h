//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL3_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL3_H

#include "integrals.h"

struct Int3y5 {
    A2SincParameters a2p;
    int type;
};

struct Int3y3 {
    A2SincParameters a2p;
    int type;
    double y5;
};

struct Int3y2 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y3;
};

struct Int3y1 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y3;
    double y2;
};

double GetInt3Core(double y1, void* par);
double GetInt3Y1(double y2, void* par);
double GetInt3Y2(double y3, void* par);
double GetInt3Y3N0(double y5, void* par);
double GetInt3Y3Nnot0(double y5, void* par);
double GetInt3Y5N0(A2SincParameters a2p, int type);
double GetInt3Y5Nnot0(A2SincParameters a2p, int type);


#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL3_H
