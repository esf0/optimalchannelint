//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL9_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL9_H

#include "integrals.h"

struct BInt9y5 {
    B2SincParameters b2p;
    int type;
};

struct BInt9y4 {
    B2SincParameters b2p;
    int type;
    double y5;
};

struct BInt9y3 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
};

struct BInt9y1 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
    double y3;
};

double GetBInt9Core(double y1, void* par);
double GetBInt9Y1(double y3, void* par);
double GetBInt9Y3(double y4, void* par);
double GetBInt9Y4N0(double y5, void* par);
double GetBInt9Y4Nnot0(double y5, void* par);
double GetBInt9Y5N0(B2SincParameters b2p, int type);
double GetBInt9Y5Nnot0(B2SincParameters b2p, int type);

#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL9_H
