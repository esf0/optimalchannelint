//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL9_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL9_H

#include "integrals.h"

struct Int9y5 {
    A2SincParameters a2p;
    int type;
};

struct Int9y4 {
    A2SincParameters a2p;
    int type;
    double y5;
};

struct Int9y3 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
};

struct Int9y1 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
    double y3;
};

double GetInt9Core(double y1, void* par);
double GetInt9Y1(double y3, void* par);
double GetInt9Y3(double y4, void* par);
double GetInt9Y4N0(double y5, void* par);
double GetInt9Y4Nnot0(double y5, void* par);
double GetInt9Y5N0(A2SincParameters a2p, int type);
double GetInt9Y5Nnot0(A2SincParameters a2p, int type);

#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL9_H
