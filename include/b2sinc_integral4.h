//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL4_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL4_H

#include "integrals.h"

struct BInt4y5 {
    B2SincParameters b2p;
    int type;
};

struct BInt4y3 {
    B2SincParameters b2p;
    int type;
    double y5;
};

struct BInt4y2 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y3;
};

struct BInt4y1 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y3;
    double y2;
};

double GetBInt4Core(double y1, void* par);
double GetBInt4Y1(double y2, void* par);
double GetBInt4Y2(double y3, void* par);
double GetBInt4Y3N0(double y5, void* par);
double GetBInt4Y3Nnot0(double y5, void* par);
double GetBInt4Y5N0(B2SincParameters b2p, int type);
double GetBInt4Y5Nnot0(B2SincParameters b2p, int type);

#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL4_H
