//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_A2SINC_INTEGRAL5_H
#define OPTIMALCHANNELINT_A2SINC_INTEGRAL5_H

#include "integrals.h"

struct Int5y5 {
    A2SincParameters a2p;
    int type;
};

struct Int5y4 {
    A2SincParameters a2p;
    int type;
    double y5;
};

struct Int5y2 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
};

struct Int5y1 {
    A2SincParameters a2p;
    int type;
    double y5;
    double y4;
    double y2;
};

double GetInt5Core(double y1, void* par);
double GetInt5Y1(double y2, void* par);
double GetInt5Y2(double y4, void* par);
double GetInt5Y4N0(double y5, void* par);
double GetInt5Y4Nnot0(double y5, void* par);
double GetInt5Y5N0(A2SincParameters a2p, int type);
double GetInt5Y5Nnot0(A2SincParameters a2p, int type);


#endif //OPTIMALCHANNELINT_A2SINC_INTEGRAL5_H
