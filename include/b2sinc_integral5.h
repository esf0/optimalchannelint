//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL5_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL5_H

#include "integrals.h"

struct BInt5y5 {
    B2SincParameters b2p;
    int type;
};

struct BInt5y4 {
    B2SincParameters b2p;
    int type;
    double y5;
};

struct BInt5y2 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
};

struct BInt5y1 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
    double y2;
};

double GetBInt5Core(double y1, void* par);
double GetBInt5Y1(double y2, void* par);
double GetBInt5Y2(double y4, void* par);
double GetBInt5Y4N0(double y5, void* par);
double GetBInt5Y4Nnot0(double y5, void* par);
double GetBInt5Y5N0(B2SincParameters b2p, int type);
double GetBInt5Y5Nnot0(B2SincParameters b2p, int type);


#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL5_H
