//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL7_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL7_H

#include "integrals.h"

struct BInt7y5 {
    B2SincParameters b2p;
    int type;
};

struct BInt7y4 {
    B2SincParameters b2p;
    int type;
    double y5;
};

struct BInt7y3 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
};

struct BInt7y1 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y4;
    double y3;
};

double GetBInt7Core(double y1, void* par);
double GetBInt7Y1(double y3, void* par);
double GetBInt7Y3(double y4, void* par);
double GetBInt7Y4N0(double y5, void* par);
double GetBInt7Y4Nnot0(double y5, void* par);
double GetBInt7Y5N0(B2SincParameters b2p, int type);
double GetBInt7Y5Nnot0(B2SincParameters b2p, int type);

#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL7_H
