//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL2_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL2_H

#include "integrals.h"

struct BInt2y4 {
    B2SincParameters b2p;
    int type;
};

struct BInt2y3 {
    B2SincParameters b2p;
    int type;
    double y4;
};

struct BInt2y2 {
    B2SincParameters b2p;
    int type;
    double y4;
    double y3;
};

struct BInt2y1 {
    B2SincParameters b2p;
    int type;
    double y4;
    double y3;
    double y2;
};

double GetBInt2Core(double y1, void* par);
double GetBInt2Y1(double y2, void* par);
double GetBInt2Y2(double y3, void* par);
double GetBInt2Y3(double y4, void* par);
double GetBInt2Y4(B2SincParameters b2p, int type);


#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL2_H
