//
// Created by esf0 on 12.02.2020.
//

#ifndef OPTIMALCHANNELINT_B2SINC_INTEGRAL3_H
#define OPTIMALCHANNELINT_B2SINC_INTEGRAL3_H

#include "integrals.h"

struct BInt3y5 {
    B2SincParameters b2p;
    int type;
};

struct BInt3y3 {
    B2SincParameters b2p;
    int type;
    double y5;
};

struct BInt3y2 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y3;
};

struct BInt3y1 {
    B2SincParameters b2p;
    int type;
    double y5;
    double y3;
    double y2;
};

double GetBInt3Core(double y1, void* par);
double GetBInt3Y1(double y2, void* par);
double GetBInt3Y2(double y3, void* par);
double GetBInt3Y3N0(double y5, void* par);
double GetBInt3Y3Nnot0(double y5, void* par);
double GetBInt3Y5N0(B2SincParameters b2p, int type);
double GetBInt3Y5Nnot0(B2SincParameters b2p, int type);


#endif //OPTIMALCHANNELINT_B2SINC_INTEGRAL3_H
